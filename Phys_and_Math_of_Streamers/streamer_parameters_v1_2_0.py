#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Streamer Parameter Package
==========================

Find streamer parameters a (radius), V (speed), Es (intrinsic field)
from L (length) and Ee (external field) using equations in the paper.

This is a part of supplemental material for a paper entitled
"Electric Streamers as a Nonlinear Instability".

To get started, try "demo()" and see its source code (running this file as a script also calls it).

To test various assumptions used in approximation (such as a_ph=a/2 etc.), run:
    test_assumptions()

To reproduce results in the paper, call
    reproduce_paper()
Then the modes will be created in "final/modes/" and max-V results will be created in
"final/pos.npz" and "final/neg.npz". You may launch several "reproduce_paper()" processes at
once (in the same directory!), they will not overwrite each other's results.

After this is finished, the negative thresholds may be calculated by calling
    calculate_negative_threshold()

Version 1.2.0: for the revised version of the paper
(slightly simplified equations, fewer assumptions).

Created on Mon Apr 1, 2019
@author: Nikolai G. Lehtinen
"""

__version_major__ = '1'
__version_minor__ = '2'
__version_revision__ = '0'
__version__ = __version_major__+'.'+__version_minor__+'.'+__version_revision__
VMarker = __version_major__+'_'+__version_minor__+'_'+__version_revision__

#%%#################################################################################################
# Preliminaries: load packages and define some utility functions

import os
import time
import numpy as np
import matplotlib.pyplot as plt
import scipy.constants as phys
from scipy.optimize import brentq

# Colorful output
_colors = \
{'black': '\x1b[30m',
 'red': '\x1b[31m',
 'green': '\x1b[32m',
 'yellow': '\x1b[33m',
 'blue': '\x1b[34m',
 'magenta': '\x1b[35m',
 'cyan': '\x1b[36m',
 'white': '\x1b[37m'}

def colorize(s,color=None,bold=False,underline=False):
    if color is not None:
        s = s + '\x1b[0m'
        if bold:
            s = '\x1b[1m' + s
        if underline:
            s = '\x1b[4m' + s
        s = _colors[color] + s
    return s

print(colorize('*** Streamer Parameter Package, v. '+__version__+' ***',color='magenta',bold=True))

def red(s):
    return colorize(s,color='red',bold=True)

def blue(s):
    return colorize(s,color='blue',bold=True)

def mkdir(dirname):
    if not os.path.isdir(dirname):
        print(red('Warning:'),'Creating directory',dirname)
        os.makedirs(dirname)

def full_as(x,fill_value):
    """Return either an array or scalar (size copied from x), filled with value fill_value"""
    return np.full(np.asarray(x).shape,fill_value,dtype=np.double)[()]

def ave(x): return (x[1:]+x[:-1])/2

def anti_diff(arr0,darr):
    "The inverse operation to np.diff for 1D arrays"
    return np.cumsum(np.hstack((arr0,darr)))

class neat_float(float):
    "Short-print of floats"
    def __str__(self):
        return "%0.5g" % self.real
    def __repr__(self):
        return self.__str__()

def latex_float(f,digits=5):
    s = '-' if (f<0) else ''
    float_str = ('{0:.'+str(int(abs(digits)))+'g}').format(abs(f))
    if "e" in float_str:
        base, exponent = float_str.split("e")
        basef = '' if base=='1' else r'{:} \times'.format(base)
        return s + basef + r'10^{{{:}}}'.format(int(exponent))
    else:
        return s + float_str

def sign_str(sign):
    return 'pos' if sign==1 else 'neg'

def get_fname(sign,Ee,L):
    "Useful for saving and loading results"
    fname =  'modes/' + sign_str(sign)
    fname += '_Ee'+str(neat_float(Ee/1e6))+'MVm_L'+str(neat_float(L/1e-3))+'mm'
    return fname

def get_grid(p=5,N=50):
    return np.sinh(np.linspace(0,p,N+1))/np.sinh(p)

#%%#################################################################################################
# The chemistry functions
N0=2.688e25 # Loschmidt constant

#% Mobility
def mue_fun(E):
    "Mobility"
    return (E/3e6)**(-.17)*0.044

def vd_fun(E):
    "Drift velocity"
    return mue_fun(E)*E

def townsend(E,ax,Ex):
    "Townsend approximation"
    return ax*np.exp(-Ex/np.abs(E))

def alphai_fun(E):
    "Ionization in Townsend approximation"
    return townsend(E,5.4e5,1.95e7)

def alphaa_fun(E):
    """Spatial attachment rate, two body (in Townsend approximation) and three-body.
    The small added term is to avoid singularity at E=0.
    """
    eta3 = 4.7778e-69*N0**2*((E/N0+1e-23)*1e4)**(-1.2749)
    return townsend(E,1.7e3,3.2e6) + eta3

# Derived functions
def nui_fun(E): return alphai_fun(E)*vd_fun(E)
def nua_fun(E): return alphaa_fun(E)*vd_fun(E)
def alphat_fun(E): return alphai_fun(E)-alphaa_fun(E)
def nut_fun(E): return alphat_fun(E)*vd_fun(E)

#%%#################################################################################################
# Keep the settable parameters to minimum, monkey-patching functions is preferred.
Prefs_default = {'mom':True, 'accurate_ns':True, 'apha':0.5, 'p_Eout':1,
             'ESmooth':False, 'EShift':0, 'NEs':200}
class PrefsClass:
    "Set the preferences"
    def __init__(self):
        self.__dict__['default'] = Prefs_default
        self.__dict__['quiet']=False
        self.reset()
    def _set(self,k,v,_info):
        self.__setattr__(k,v)
        if _info: print('Set preferences: ', self)
    def set(self,**newprefs):
        self.__dict__['quiet']=True
        for k,v in newprefs.items():
            self._set(k,v,False)
        self.__dict__['quiet']=False
        print(self)
    def __setattr__(self,k,v):
        if k in self.default:
            self.__dict__[k] = v
        else:
            raise Exception('Preference "'+k+'" does not exist')
        if not self.quiet: print(self)
    def reset(self):
        self.__dict__.update(self.default)
        print(self)
    def __repr__(self):
        return 'Preferences are set to: ' + ', '.join(
                '%s=%s' % (k,repr(self.__dict__[k])) for k in sorted(self.default))
    pass

Prefs = PrefsClass()

#%%#################################################################################################
class PhotoionizationClass:
    "Photoionization"
    def __init__(self):
        "Only primary constants"
        self.Lam2 = 2e-3
        self.Lam1 = 35e-6
        self.pq = 60
        self.Aphot = 0.1*self.pq/(self.pq+760)
        # The distance parameter for photoionization
        self.xb = np.hstack((np.arange(1000)*2e-8,np.arange(100,1000)*2e-7,
                np.arange(100,1000)*2e-6,np.arange(100,1000)*2e-5,
                np.arange(100,1001)*2e-4))
        self.reset()
    def reset(self):
        "Derived constants"
        self.dx = np.diff(self.xb)
        self.xc = ave(self.xb)
        # Integrate over the area of the streamer head
        self.zh2pir_intb = -np.flip(anti_diff(
                0,np.flip(self._zheleznyak_2pir(self.xc)*self.dx)))
    def _zheleznyak_2pir(self,r):
        "A*F(r)*2*pi*r"
        return self.Aphot*(np.exp(-r/self.Lam2)-np.exp(-r/self.Lam1))\
            /r**2/(2*np.log(self.Lam2/self.Lam1))
    def zheleznyak_1D(self,z,a):
        "pi*a**2*Feff(z)"
        return np.interp(np.sqrt(z**2+a**2),self.xb,self.zh2pir_intb)-\
            np.interp(z,self.xb,self.zh2pir_intb)

Photo = PhotoionizationClass()

#%%#################################################################################################
class IonizationIntegralClass:
    """Integrated spatial impact ionization rate,
    called 'ionization integral' Psi(E).
    Ion density is ni=(eps0/e)*Psi(E)
    Electron density is n=V/(V+vd)*ni
    See Lagarkov and Rutkevich, 1994, p. 62, eq (3.1.12)."""
    def __init__(self):
        self.Eb = np.linspace(0,1e8,1000001)
        self.dE = np.diff(self.Eb)
        self.Ec = ave(self.Eb)
        self.dPsi = alphat_fun(self.Ec)*self.dE
        self.Psib = anti_diff(0,self.dPsi)
        # Find where the minimum, where zero
        imin = np.argmin(self.Psib)
        self.Ecrit = self.Eb[imin]
        #self.Ezero = np.interp(0,self.Psib[imin:],self.Eb[imin:])
        print('Critical field (minimum of Psi) =',neat_float(self.Ecrit/1e6),'MV/m')
        #print('Necessary condition: Em >',neat_float(self.Ezero/1e6),'MV/m')
        print('Max error in ns =',neat_float(-(phys.epsilon_0/phys.e)*self.Psib[imin]),'m^{-3}')
    def __call__(self,E):
        "Electron density in a flat front from the max value of E field"
        return np.interp(E,self.Eb,self.Psib,left=np.nan,right=np.nan)

Psi_fun = IonizationIntegralClass()

#%%#################################################################################################
# Method of moments results (or not)

def rod_cap_la(La):
    "Fit the field as eta/(1+x/l), for distances 0<x<1"
    if Prefs.mom:
        return 0.39553749 -0.58940288/(La + 2.30673538)
    else:
        return full_as(La,1/np.sqrt(8))

def rod_cap_lincharge_neck(La):
    """Linear charge at the neck (distance of 1 from tip).
    The dimensional value is lambda*(Ee-Es)*a*eps0.
    This function is not used in the calculations and is present only for demonstration."""
    if Prefs.mom:
        return 2.70492454*(La-1) + 7.43102822*np.sqrt(La-1)
    else:
        return 4*np.pi*eta_fun(La)*rod_cap_la(La)

def eta_fun(La):
    "Field enhancement at the tip"
    if Prefs.mom:
        eta = np.asarray(2+0.56*(2*La)**0.92)
    else:
        eta = np.asarray(2+La)
    #eta = np.asarray(2+La)
    eta[np.asarray(La)<1]=np.nan # There is no rod
    return eta[()]

#%%#################################################################################################
# The relations between parameters

def get_iLatt(**params):
    """Inverse attenuation length. Not used inside the package, only for user use."""
    Es = params['Es']
    Vr = params['V'] + params['sign']*vd_fun(Es)
    iLatt = np.asarray(-nut_fun(Es)/Vr)
    iLatt[iLatt<0] = 0
    iLatt[np.isnan(iLatt)] = 0
    return iLatt[()]

def get_Ef(**params):
    L=params['L']; a=params['a']; Ee=params['Ee']; Es=params['Es'];
    Ef = eta_fun(L/a)*(Ee-Es)+Ee
    return Ef

def get_nsnorm(**params):
    "Calculate nsnorm == ns*e/eps0"
    Es = params['Es']
    V = params['V']
    if 'Ef' in params:
        Ef = params['Ef']
    else:
        Ef = get_Ef(**params)
    nsnorm0 = Psi_fun(Ef)-Psi_fun(Es)
    if Prefs.accurate_ns:
        return nsnorm0*V/(V+params['sign']*vd_fun(Es))
    else:
        return nsnorm0

def update_Es(**params):
    L=params['L']; a=params['a']; V=params['V']
    Ee=params['Ee']; Es=params['Es'];
    nsnorm = get_nsnorm(**params)
    # Use the total current continuity Jc = Jd
    # Jc = e*ns*mu(Es)*Es; Jd = -eps0*V*E' ; E-Ee = eta*(Ee-Es)/(1+xi/l)^p; p=1
    Esnew = np.asarray(Prefs.p_Eout*V*eta_fun(L/a)*(Ee-Es)/(nsnorm*mue_fun(Es)*rod_cap_la(L/a)*a))
    Esnew[np.asarray(nsnorm)<0] = np.nan
    return Esnew[()]

class PancheshnyiSolverClass:
    "Calculate streamer velocity from Pancheshnyi et al [2001] equation"
    def __init__(self):
        self.verbose = False
    def initialize(self,**params):
        self.params = params
        Ee = params['Ee']
        self.Es = params['Es']
        assert Ee>self.Es
        #L = params['L']
        self.sign = params['sign']
        assert self.sign in [-1,1]
        a = params['a']
        self.Ph = Photo.zheleznyak_1D(Photo.xc,a*Prefs.apha)
        self.Ef = get_Ef(**params)
        params['Ef']=self.Ef
        if not Prefs.ESmooth:
            self.Ec = Estreamer(Photo.xc,**params)
            self.nut = nut_fun(self.Ec)
            self.vd = vd_fun(self.Ec)
        self.Vmax = 1e8 # Warning: this is getting close to speed of light!
        self.Vmin = max(0,-self.sign*vd_fun(self.Ef)) + 1e2 # For negative
        return self
    def contrib(self,V):
        if Prefs.ESmooth:
            d = get_d(self.Ef,V,self.sign)
            self.Ec = Estreamer(Photo.xc,d=d,Ef=self.Ef,**self.params)
            self.nut = nut_fun(self.Ec)
            self.vd = vd_fun(self.Ec)
        Vvis = V+self.sign*self.vd
        growth = self.nut/Vvis
        growth[Vvis<=0]=np.inf*np.sign(self.nut[Vvis<=0])
        stot = self.Ph
        return stot*np.exp(ave(anti_diff(0,growth*Photo.dx)))
    def fun(self,V):
        #self.calls += 1
        #t0 = time.time()
        one = np.sum(self.contrib(V)*Photo.dx)
        #self.T += time.time()-t0
        return np.log(one)
    def get_V(self):
        # There is only one solution
        if self.fun(self.Vmin)*self.fun(self.Vmax)>0:
            return np.nan
        else:
            return brentq(self.fun, self.Vmin, self.Vmax)
    pass

VFinder = PancheshnyiSolverClass()

#%%#################################################################################################
# The calculations!

def find_Es(Esmin=None, Esmax=None, **params):
    "New in 1.1: binary search instead of interpolation"
    a=params['a']; L=params['L']; Ee=params['Ee']; sign=params['sign']
    NEs = Prefs.NEs
    if Esmin is None:
        Esmin = Ee/NEs
    if Esmax is None:
        Esmax = Ee*(NEs-1)/NEs
    #Es0arr = np.linspace(0,1,NEs+1)[1:-1]*Ee
    Es0arr = np.linspace(Esmin,Esmax,NEs+1)
    def local_V(Es):
        if np.isnan(Es): return np.nan
        return VFinder.initialize(Es=Es,Ee=Ee,a=a,L=L,sign=sign).get_V()
    def dEs_fun(Es):
        V = local_V(Es)
        if np.isnan(V): return np.nan
        return np.log(update_Es(V=V,Es=Es,Ee=Ee,a=a,L=L,sign=sign)/Es)
    dEs = np.nan*np.ones(Es0arr.shape)
    for k,Es in enumerate(Es0arr):
        dEs[k] = dEs_fun(Es)
    ii = np.where(dEs[1:]*dEs[:-1]<=0)[0] # only selects where Es1 is not a nan
    Essol = np.array([brentq(dEs_fun, Es0arr[i], Es0arr[i+1]) for i in ii])
    Vsol = np.array([local_V(Es) for Es in Essol])
    return Essol,Vsol

def streamer_modes(aarr,L=None,Ee=None,sign=None,display_progress=True):
    assert L is not None and Ee is not None and sign is not None
    if (aarr>L).any():
        print('Warning: a>L values will be discarded')
    aarr = aarr[aarr<=L]
    Na = len(aarr)
    Es_list = []
    V_list = []
    Nsol = 1
    if display_progress:
        progress_k = 0
    for k,a in enumerate(aarr):
        if display_progress:
            # Display progress
            if 10*k/Na >= progress_k:
                print('X',end='',flush=True)
                progress_k += 1
        Es,V = find_Es(a=a,L=L,Ee=Ee,sign=sign)
        Es_list.append(Es)
        V_list.append(V)
        isol = len(Es)
        Nsol = max(isol,Nsol)
    Es_full = np.nan*np.zeros((Nsol,Na))
    V_full = Es_full.copy()
    if Nsol>0:
        for k,a in enumerate(aarr):
            Es = Es_list[k]
            l = len(Es)
            if l>0:
                Es_full[:l,k]=Es
                V_full[:l,k]=V_list[k]
    if display_progress:
        print()
    return {'sign':sign, 'Ee':Ee, 'L':L, 'a':aarr, 'Es':Es_full, 'V':V_full}

#%%#################################################################################################
# Functions needed to calculate other parameters, not essential for V and Es

def K_Ef(d,L,a):
    "Not used in calculations! Returns Em/Ef and xim/a"
    dl = d/a/rod_cap_la(L/a)
    t = np.asarray(dl*np.log(1/dl))
    t[(dl>1) & (dl<0)]=np.nan
    u = np.exp(t)
    xm = (u-1)/dl
    return 1/u/(1+np.exp(-xm)),xm*d/a

def sigmoid(x):
    return (np.tanh(x/2)+1)/2
    
def Estreamer(xi,smooth=None,**params):
    """Streamer electric field.
    Options smooth=False calculates in the approximation d==0; use smooth=True for d>0.
    The smooth version is compatible with values of Em and xim, calculated by K_Ef().
    See the Appendix in the paper. This function also works for x<0.
    To calculate d, we need to know V, which, however, is not needed in non-smooth version."""
    if smooth is None:
        smooth = Prefs.ESmooth
    Ee = params['Ee']
    #Ee=params['Ee_fun'](params['L']);
    L=params['L']; a=params['a']; Es=params['Es']
    if 'Ef' in params:
        Ef = params['Ef']
    else:
        Ef = get_Ef(**params)
    l = a*rod_cap_la(L/a)
    # Ef = get_dEf(**params) + Ee
    xi = np.asarray(xi)
    if not smooth:
        E = np.asarray(full_as(xi,Es))
        E[xi>=0] = (Ef-Ee)/(1+xi[xi>=0]/l)**Prefs.p_Eout + Ee
    else:
        d = get_d(Ef,params['V'],params['sign'])
        #print('d =',neat_float(d/1e-3),'mm')
        xis = np.asarray(xi-Prefs.EShift*d)
        xx = 1 + xis/l
        y1 = np.zeros(xis.shape)
        # y1 is continuous thru xi=0
        y1[xis<0] = 1
        y1[xis>=0] = 1/xx[xis>=0]**Prefs.p_Eout # to account for a drop in alpha(xi)
        y2 = (Es-Ee) + (Ef-Es)*sigmoid(xis/d)
        E = y1*y2
        E[xi>=0] += Ee
        E[xi<0] += Ee
    return E

def get_d(E,V,sign):
    "Not used in calculations! Returns the thickness of the streamer front d."
    return (V+sign*vd_fun(E))/nut_fun(E)

def expand_results(**params):
    "Add more stuff to results obtained by streamer_modes"
    Ee = params['Ee']
    L = params['L']
    sign = params['sign']
    a = params['a']
    V = params['V']
    Nsol,Na = V.shape
    Es = params['Es']
    Ef = get_Ef(**params)
    d = get_d(Ef,V,sign)
    EmEf,xima = K_Ef(d,L,a)
    Em = Ef*EmEf
    xim = xima*a
    ns = get_nsnorm(**params)*phys.epsilon_0/phys.e
    xeq = np.nan*np.ones(V.shape)
    Exeq = np.nan*np.ones(V.shape)
    for k in range(Na):
        for l in range(Nsol):
            if np.isnan(V[l,k]):
                continue
            VFinder.initialize(a=a[k],Ee=Ee,L=L,sign=sign,Es=Es[l,k])
            xeq[l,k] = np.interp(0.5,anti_diff(0,VFinder.contrib(V[l,k])*Photo.dx),Photo.xb)
            Exeq[l,k] = np.interp(xeq[l,k],Photo.xc,VFinder.Ec)
    result = {'xim':xim,'xeq':xeq,'Exeq':Exeq,'Ef':Ef,'Em':Em,'ns':ns,'d':d}
    result.update(**params)
    return result

def apply_maxV(**params):
    """Apply the max-V principle to results of expand_results.
    A more accurate way to do it is possible, but will be implemented in the future."""
    Ee = params['Ee']
    L = params['L']
    sign = params['sign']
    resnan = {'sign':sign, 'Ee':Ee, 'L':L,
              'a':np.nan,'V':np.nan,'Es':np.nan,'xim':np.nan,'xeq':np.nan,'Exeq':np.nan,
              'Ef':np.nan,'Em':np.nan,'ns':np.nan,'d':np.nan}
    a = params['a']
    V = params['V'][0] # Highest branch by construction
    Na = len(a)
    da = np.diff(a)
    ac = ave(a)
    if np.isnan(V).all():
        return resnan
    i = np.nanargmax(V)
    dVda = np.diff(V)/da # at i, must change sign
    # Less accurate
    if False:
        if i<1 or i>Na-2: return resnan
        # Same interpolation as in find_Es
        #amax = (dVda[i+1]*ac[i]-dVda[i]*ac[i+1])/(dVda[i+1]-dVda[i])
        amax = np.interp(0,-dVda[i:i+2],ac[i:i+2])
    else:
        if i<1 or i>Na-3: return resnan
        # Very primitive, just find derivative and linear fit
        amax = np.interp(0,-dVda[i-1:i+3],ac[i-1:i+3])
    Vmax = np.interp(amax,a,V)
    resmax = {'sign':sign, 'Ee':Ee, 'L':L,
              'a':amax,'V':Vmax,'Es':np.interp(amax,a,params['Es'][0]),
              'xim':np.interp(amax,a,params['xim'][0]),
              'xeq':np.interp(amax,a,params['xeq'][0]),
              'Exeq':np.interp(amax,a,params['Exeq'][0]),
              'Ef':np.interp(amax,a,params['Ef'][0]),
              'Em':np.interp(amax,a,params['Em'][0]),
              'ns':np.interp(amax,a,params['ns'][0]),
              'd':np.interp(amax,a,params['d'][0])}
    return resmax

#%%#################################################################################################
# Batch calculations

def agrid(L):
    "A convenient grid for radii, depending on L. Monkey-patch if needed."
    return get_grid()[1:]*L

def batch_streamer_modes(sign,Ee_s,L_s,dirname,erase=False):
    "Calculate the streamer mode structure for many values of Ee and L"
    if dirname[-1]!='/':
        dirname += '/'    
    for L in L_s:
        for Ee in Ee_s:
            prefix = dirname+get_fname(sign,Ee,L)
            fname = prefix + '.npz'
            flock = prefix + '.lock'
            if not erase and (os.path.isfile(fname) or os.path.isfile(flock)):
                print('Skipping',fname)
            else:
                # Place a lock on the file
                with open(flock, 'w') as f:
                    f.write('')
                print('Calculating',fname)
                with np.errstate(all='ignore'):
                    results = streamer_modes(agrid(L),L=L,Ee=Ee,sign=sign)
                    results = expand_results(**results)
                np.savez(fname,**results)
                os.remove(flock)
                
def batch_apply_maxV(sign,Ee_s,L_s,dirname,verbose=False):
    """Make a summary of results, applying max-V criterion.
    Usage example:
        sign = 1
        batch_streamer_modes(sign, Ee_s, L_s, dirname)
        # -- may be split into several runs, even on different CPU
        # ...
        resmaxv = batch_apply_maxV(sign, Ee_s, L_s, dirname)
        np.savez(dirname + '/pos.npz',**resmaxv)
    """
    if dirname[-1]!='/':
        dirname += '/'
    a_s = np.nan*np.zeros((len(Ee_s),len(L_s)))
    V_s = np.nan*np.zeros(a_s.shape)
    Es_s = np.nan*np.zeros(a_s.shape)
    xim_s = np.nan*np.zeros(a_s.shape)
    xeq_s = np.nan*np.zeros(a_s.shape)
    Exeq_s = np.nan*np.zeros(a_s.shape)
    Ef_s = np.nan*np.zeros(a_s.shape)
    Em_s = np.nan*np.zeros(a_s.shape)
    ns_s = np.nan*np.zeros(a_s.shape)
    d_s = np.nan*np.zeros(a_s.shape)
    for kL, L in enumerate(L_s):
        if verbose:
            print('L =',L)
        for kE, Ee in enumerate(Ee_s):
            fname = dirname + get_fname(sign,Ee,L) + '.npz'
            try:
                results = np.load(fname)
            except:
                if verbose:
                    print('File',fname,'is missing')
                continue
            resmax = apply_maxV(**results)
            a_s[kE,kL] = resmax['a']
            V_s[kE,kL] = resmax['V']
            Es_s[kE,kL] = resmax['Es']
            xim_s[kE,kL] = resmax['xim']
            xeq_s[kE,kL] = resmax['xeq']
            Exeq_s[kE,kL] = resmax['Exeq']
            Ef_s[kE,kL] = resmax['Ef']
            Em_s[kE,kL] = resmax['Em']
            ns_s[kE,kL] = resmax['ns']
            d_s[kE,kL] = resmax['d']
    return dict(sign=sign,Ee=Ee_s,L=L_s,a=a_s,V=V_s,Es=Es_s,xim=xim_s,xeq=xeq_s,Exeq=Exeq_s,
             Ef=Ef_s,Em=Em_s,ns=ns_s,d=d_s)

#%%#################################################################################################
# Demo of what the package can do and simplified interface

def calculate_streamer_modes(sign=None,Ee=None,L=None):
    "Brain-free calculation of streamer modes (wrapper for streamer_modes)."
    assert Ee is not None and L is not None and sign is not None
    with np.errstate(all='ignore'):
        res = streamer_modes(agrid(L), L=L, Ee=Ee, sign=sign)
        result = expand_results(**res)
    return result

def calculate_streamer_parameters(sign=None,Ee=None,L=None):
    "Brain-free calculation of all streamer parameters"
    results = calculate_streamer_modes(Ee=Ee,L=L,sign=sign)
    return apply_maxV(**results)

def basic_calculation(Ee=None,L=None):
    "How to use the package, used in the demo"
    print('Calculating streamer modes for L =',neat_float(L/1e-3),'mm, Ee =',neat_float(Ee/1e6),
          'MV/m, please wait ...')
    t0=time.time()
    print(red('Positive: '),end='')
    positive = calculate_streamer_modes(Ee=Ee,L=L,sign=1)
    print(blue('Negative: '),end='')
    negative = calculate_streamer_modes(Ee=Ee,L=L,sign=-1)
    print('... Done! It took', neat_float(time.time()-t0),'sec')
    return (positive,negative)

def basic_plot(result, plotmax=False, legend=False, style='-'):
    color = 'red' if result['sign']==1 else 'blue'
    lines = plt.plot(result['a']/1e-3, result['V'].T/1e6, style, color=color)
    if legend:
        lines[0].set_label(('positive' if result['sign']==1 else 'negative') + 
              r', $L = ' + latex_float(result['L']/1e-3) + r'$ mm, $E_e = ' + \
              latex_float(result['Ee']/1e6) + r'$ MV/m')
    if plotmax:
        rmax = apply_maxV(**result)
        plt.plot(rmax['a']/1e-3, rmax['V']/1e6,'o', color=color)
    return lines[0]

def basic_figure(results):
    plt.figure()
    for result in results: basic_plot(result, plotmax=True, legend=True)
    plt.grid(True)
    plt.xlabel(r'$a$, mm')
    plt.ylabel(r'$V$, Mm/s')
    #L = results[0]['L']
    #Ee = results[0]['Ee']
    plt.title(r'Streamer modes')
    #for $L = ' + latex_float(L/1e-3) + r'$ mm, $E_e = ' + \
    #          latex_float(Ee/1e6) + r'$ MV/m')
    plt.legend()
    plt.show()

def demo(Ee=1.5e6,L=80e-3):
    "A simple demonstration of what the package can do: a figure from the paper."
    Prefs.reset()
    positive,negative = basic_calculation(Ee=Ee,L=L)
    basic_figure([positive,negative])
    
def test_assumptions(Ee=1.5e6,L=80e-3):
    "The same as 'demo()', but test variations in the assumptions"
    plt.figure()
    Prefs.reset()
    positive,negative = basic_calculation(Ee=Ee,L=L)
    [basic_plot(result, plotmax=True, legend=True) for result in [positive,negative]]
    # Try a different formula for n_s
    Prefs.set(accurate_ns=False)
    positive_ns,negative_ns = basic_calculation(Ee=Ee,L=L)
    lines_ns = [basic_plot(result, style='--') for result in [positive_ns,negative_ns]]
    lines_ns[0].set_label(r'Simpler expression for $n_s$')
    # Try a different ratio a_ph/a
    Prefs.set(accurate_ns=True,apha=1)
    positive_a1,negative_a1 = basic_calculation(Ee=Ee,L=L)
    lines_a1 = [basic_plot(result, style='-.') for result in [positive_a1,negative_a1]]
    lines_a1[0].set_label(r'$a_{ph}=a$ instead of $a_{ph}=a/2$')
    Prefs.set(apha=0.5,mom=False)
    positive_nom,negative_nom = basic_calculation(Ee=Ee,L=L)
    lines_mom = [basic_plot(result, style=':') for result in [positive_nom, negative_nom]]
    lines_mom[0].set_label(r'Not using MoM results')
    plt.grid(True)
    plt.xlabel(r'$a$, mm')
    plt.ylabel(r'$V$, Mm/s')
    plt.title(r'Streamer modes for $L = ' + latex_float(L/1e-3) + r'$ mm, $E_e = ' + \
              latex_float(Ee/1e6) + r'$ MV/m')
    plt.legend()

#%%#################################################################################################
# Reproduce the paper results

def paper_result_values():
    """Auxiliary function with full tables of inputs and the output directory"""
    dirname = 'final_v' + VMarker + '/'
    Eearr = np.arange(0.1,3.01,0.05)*1e6
    Larr = np.arange(5,200.1,5)*1e-3
    return dirname, Eearr, Larr

def calculate_negative_threshold(Niter=5,erase=False):
    """Binary search of negative threshold.
    Relatively computationally intensive, must be called AFTER all paper parameter calculations
    are done."""
    dirname, _o, _o = paper_result_values()
    resmaxvn = dict(np.load(dirname + 'neg.npz'))
    Larr = resmaxvn['L']
    Eearr = resmaxvn['Ee']
    # The rough estimate first
    Ethreshn = np.nan*np.ones((len(Larr),2))
    for kL,L in enumerate(Larr):
        tmp = np.where(~np.isnan(resmaxvn['a'][:,kL]))[0]
        if len(tmp)==0:
            continue
        i = tmp[0]
        Ethreshn[kL,:] = Eearr[i-1:i+1]
        print('L=',L,': Ethresh =',Ethreshn[kL]/1e6)
    # Now, the real work
    threshdir = dirname + 'Ethresh/'
    mkdir(threshdir)
    for kL,L in enumerate(Larr):
        prefix = threshdir + 'L' + str(neat_float(L/1e-3)) + 'mm'
        fname = prefix + '.npz'
        flock = prefix + '.lock'
        if not erase and (os.path.isfile(fname) or os.path.isfile(flock)):
            print('Skipping',fname)
            continue
        # Place a lock
        print('*** L =',neat_float(L/1e-3),'mm ***')
        with open(flock, 'w') as f:
            f.write('')
        Elo,Ehi = Ethreshn[kL,:]
        if np.isnan(Ethreshn[kL,:]).all():
            np.savez(fname,Elo=Elo,Ehi=Ehi)
            os.remove(flock)
            continue
        # Load the results which are already calculated for Ehi
        resthresh = dict(np.load(dirname + get_fname(-1,Ehi,L) + '.npz'))
        # Perform binary search
        for kiter in range(Niter):
            Eme = (Elo + Ehi)/2
            with np.errstate(all='ignore'):
                print('Calculating at Ee =',neat_float(Eme/1e6),'MV/m')
                results = streamer_modes(agrid(L),L=L,Ee=Eme,sign=-1)
            if np.isnan(results['V']).all():
                Elo = Eme
            else:
                with np.errstate(all='ignore'):
                    resthresh = expand_results(**results)
                Ehi = Eme
            print('Narrowed to [',neat_float(Elo/1e6),',',neat_float(Ehi/1e6),'] MV/m')
        np.savez(fname,Elo=Elo,Ehi=Ehi)
        np.savez(prefix + '_modes.npz',**resthresh)
        resthreshmax = apply_maxV(**resthresh) # experimental, doesn't always work
        np.savez(prefix + '_maxV.npz',**resthreshmax)
        os.remove(flock)
    # Finally, the summary (ideally, must also check for locks, but too lazy)
    Ethreshnfine = np.nan*np.ones((len(Larr),2))
    for kL,L in enumerate(Larr):
        fname = threshdir + 'L' + str(neat_float(L/1e-3)) + 'mm.npz'
        tmp = dict(np.load(fname))
        Ethreshnfine[kL,:] = (tmp['Elo'],tmp['Ehi'])
    np.savez(threshdir + 'Ethreshn.npz',L=Larr,E=Ethreshnfine)
    return Larr, Ethreshnfine

def reproduce_paper():
    Prefs.reset()
    dirname, Eearr, Larr = paper_result_values()
    assert dirname[-1]=='/'
    mkdir(dirname+'modes/')
    # The long calculations
    for sign in [1,-1]:
        # This may be run in parallel by several processes (uses locks on files)
        batch_streamer_modes(sign,Eearr,Larr,dirname)
    # Make summary (ideally, must first check for locks)
    for sign in [1,-1]:
        print('sign =',sign)
        resmaxv = batch_apply_maxV(sign,Eearr,Larr,dirname)
        np.savez(dirname + sign_str(sign) + '.npz', **resmaxv)
    # Finally, do not forget to calculate the negative threshold!
    # calculate_negative_threshold()

#%%#################################################################################################
# Run as a script

if __name__=='__main__':
    demo(Ee=1.5e6,L=80e-3)
