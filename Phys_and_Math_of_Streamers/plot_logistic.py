#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar  5 09:40:53 2020

Plot the flat front shape (sigmoid)

@author: nleht
"""

#%% Preliminaries
LANGUAGE='eng'        # choose the language of the Figures: 'eng' or 'rus'
save_figs = True      # set to False to skip creating figure files
save_format = '.png'  # figure format

import os
import numpy as np
import matplotlib.pyplot as plt
from utils import nice_figures, make_dir
nice_figures()


#%%
frame = [0.1,0.28,0.87,0.65] # left, bottom, width, height

fig = plt.figure(figsize=(8,2))
xmax = 6
x = np.linspace(-xmax,xmax,1001)
ax = fig.add_axes(frame)
ax.plot(x,(1+np.tanh(x/2))/2)
ax.plot(x,(1-np.tanh(x/2))/2)
ax.set_xlim(-xmax,xmax)
ax.legend([r'$E/E_f$',r'$n/n_s$'])
ax.set_xlabel(r'$\xi/d$')
ax.set_ylabel(r'$E/E_f, n/n_s$')
if save_figs:
    figdir = 'figures_'+LANGUAGE
    make_dir(figdir)
    fig.savefig(os.path.join(figdir, 'logistic' + save_format))

