#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 17 14:55:19 2017

Chemistry for streamer simulations

@author: nle003
"""

#%% Preliminaries
LANGUAGE='eng'        # choose the language of the Figures: 'eng' or 'rus'
save_figs = True      # set to False to skip creating figure files
save_format = '.png'  # figure format

import os
import numpy as np
import matplotlib.pyplot as plt
import air_coefs as AIR
from utils import nice_figures, make_dir
nice_figures()

No=AIR.N0
mueo = 0.05 # electron mobility at E_0
muio = 5e-3*mueo # fits the best ML97 value
Scro=1e7 # cosmic ray source, pairs/m^3/s [Hirsh+Osken 1978, p. 221 gives 7-20 per cm^3 per sec]

numax=np.inf # nui>4e10 is necessary for streamers, see page 8 in the notebook #14

#%% Define functions
def nui_fun(E):
    EN=abs(E)/No
    res = No*AIR.nuion_N_ML97(EN) #No*alpha_N_ML97(EN)*We_ML97(EN)
    #res = No*AIR.nuion_N_P96(EN)
    #res = nui
    if np.isscalar(E):
        if res>numax: res = numax
    else:
        res[res>numax]=numax
    return res

#%%
def nua_fun(E):
    EN = abs(E)/No
    #res = air_coefs.nuatt_P96_GPI92(abs(E),No)
    #res = nuatt2_N_ML97(EN)*No + 0.1*nuatt3_N2_ML97(EN)*No**2
    res = AIR.nuatt2_N_ML97(EN)*No+AIR.nuatt3_N2_ML97(EN)*No**2
    #res = nuatt_ML97(abs(E),No)
    #res = No*eta2_N_LG12(EN)*We_ML97(EN)
    if np.isscalar(E):
        if res>numax: res = numax
    else:
        res[res>numax]=numax
    return res
#def nua_fun2(E): return nuatt_P96_GPI92(E,No)
def nud_fun(E): return AIR.nudetach_LG12(abs(E),No)


def mue_fun(E): return AIR.mueN_ML97(np.abs(E)/No)/No

def Ve_fun(E): return -mueo*E

def Vp_fun(E): return muio*E

def Vn_fun(E): return -muio*E

def D_fun(E):
    return AIR.DN_ML97(np.abs(E)/No)/No

bete = 2e-13
betn = 1e-13+No*1e-36

#%%
fconst = plt.figure(100) #,figsize=(6,4))
fconst.clear()
E=np.linspace(0,4e6,1000)
axconst = fconst.add_subplot(111)
axconst.semilogy(E/1e6,nui_fun(E))
axconst.semilogy(E/1e6,nua_fun(E))
#axconst.semilogy(E/1e6,nud_fun(E))
axconst.set_ylim(1e6,1e9)
axconst.set_xlim(0,4)
if LANGUAGE=='rus':
    axconst.set_xlabel(r'$E$, МВ/м')
    axconst.set_ylabel(r'$\nu$, с$^{-1}$')
elif LANGUAGE=='eng':
    axconst.set_xlabel(r'$E$, MV/m')
    axconst.set_ylabel(r'$\nu$, s$^{-1}$')
axconst.legend([r'$\nu_i$',r'$\nu_a$'])
axconst.grid(True)
if save_figs:
    figdir = 'figures_'+LANGUAGE
    make_dir(figdir)
    fconst.savefig(os.path.join(figdir, 'nuia'+save_format))
    
