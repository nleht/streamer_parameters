#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 28 10:07:25 2019

Plots for paper "Physics and Mathematics of Electric Streamers".

Make sure you run calculate_spm.py first.

@author: Nikolai G. Lehtinen
"""

#%% Preliminaries
LANGUAGE='eng'        # choose the language of the Figures: 'eng' or 'rus'
save_figs = True      # set to False to skip creating figure files
save_format = '.pdf'  # figure format
sign = -1             # only in 'plot_spm.py': streamer polarity (1 or -1) to plot

import os
import numpy as np
import matplotlib.pyplot as plt
import streamer_parameters_v1_2_0 as SP
from utils import place_letter, index_interval, nice_figures
nice_figures()

SP.Prefs.reset()

frame_title = [0.13,0.14,0.84,0.77] # left, bottom, width, height
frame_notitle = [0.13,0.14,0.84,0.8] # left, bottom, width, height, no space for title

path_to_data = 'spm_work'

#%% Load all the "true" data
dirname,_o,_o = SP.paper_result_values() # 'final/'
dirname = os.path.join(path_to_data, dirname)
print('*** Using data directory:',dirname)
#figdirname = dirname + 'figures/'
figdirname = 'figures_'+LANGUAGE+'/'
signstr = SP.sign_str(sign)
resmaxv = dict(np.load(os.path.join(dirname, signstr + '.npz')))
do_Va_plots = False # A lot of plots of V(a) dependence which is usually not needed
prefix = figdirname + signstr + '_'

#%% Some shortcuts for units
if LANGUAGE=='eng':
    Mms = 'Mm/s'
    MVm = 'MV/m'
    mm = 'mm'
    m3 = r'm$^{-3}$'
elif LANGUAGE=='rus':
    Mms = 'Мм/с'
    MVm = 'МВ/м'
    mm = 'мм'
    m3 = r'м$^{-3}$'
else:
    raise Exception('unknown language')

#%% Part 1 chosen values (must be loaded for part 2, too)
if sign==1:
    Lmode = np.array([120e-3])
    Eemode = np.array([0.4,0.6,0.8,1])*1e6
    alims = np.array([10e-3]) # maximum "a" to plot, dependent on L
else:
    Lmode = np.array([])
    Eemode = np.array([])
    alims = np.array([])

#%%#################################################################################################
# PART ONE: Plot only selected mode sets

# Other preferences
ascale = 1e-3
if not os.path.isdir(figdirname):
    print('Warning: Creating directory',figdirname)
    os.makedirs(figdirname)

prefixa = figdirname + 'a_' + signstr + '_'

scale={'V':1e6,'Es':1e6,'ns':1,'xeq':1e-3,'Em':1e6,'Exeq':1e6,'d':1e-3}
symbol = {'V':'V','Es':r'E_s','ns':r'n_s','xeq':r'\xi_p','Exeq':r'E(\xi_p)','d':'d'}
unit = {'V':Mms,'Es':MVm,'ns':m3,'xeq':mm,'Em':MVm,'Exeq':MVm,'d':mm}
ylim = {'ns':((0,2e19),{}),'Em':((4,12),{}),'V':((),{'bottom':0}),'Es':((),{'bottom':0})}
dimylim = {'xeq':(0,1)}
do_plot_var = {'V':True,'Es':True,'Em':True,'ns':True,'xeq':True,'Exeq':True,'d':True}
do_plot_legend = {'V':True,'Es':True,'Em':False,'ns':False,'d':False,'xeq':True,'Exeq':False}
plot_letter = {'Es':'a','ns':'c','Em':'b','d':'d','xeq':'a','Exeq':'b'}
plot_letter_loc = {'Es':'sw','ns':'sw','Em':'sw','d':'se','xeq':'sw','Exeq':'sw'}
#do_plot_var = {'vs':True,'Es':True,'Em':True,'ns':True,'xeq':True,'Exeq':True,'d':True}
do_ratio = {'xeq':True,'d':False}

do_title = (len(Lmode)>1)
plot_var = [(kvar,varname) for kvar,varname in enumerate(do_plot_var) if do_plot_var[varname]]

#%%
if do_Va_plots:
    for kL, L in enumerate(Lmode):
        print('L=',L)
        #a = np.linspace(4,200,197)*L/Lscale[sign]
        fnum = {}
        ax = {}
        for kvar, varname in plot_var:
            fnum[varname] = 1000*(kL+1) + 11 + kvar
            print('Figure ',fnum[varname],'with',varname)
            fig = plt.figure(fnum[varname])
            fig.clear()
            ax[varname]=fig.add_axes(frame_title if do_title else frame_notitle)
        for kE,Ee in enumerate(Eemode):
            plt.figure(fnum['V'])
            color=next(plt.gca()._get_lines.prop_cycler)['color']
            fname = dirname + SP.get_fname(sign,Ee,L) + '.npz'
            print('Loading',fname)
            try:
                results = np.load(fname)
            except:
                print('File',fname,'is missing')
                continue
            resmax = SP.apply_maxV(**results)
            amax = resmax['a']
            
            a = results['a']
            ia = index_interval(0,alims[kL],a)
            a = a[ia]
            
    #        V = results['V'][0]
    #        Es = results['Es'][0]
    #        d=results['d'][0]
            Ef = results['Ef'][0]
    #        Em = results['Em'][0]
    #        ns = results['ns'][0]
    #        Exeq = results['Exeq'][0]
    #        xeq = results['xeq'][0]
    
            for kvar,varname in plot_var:
                var = results[varname][0][ia]
                varmax = resmax[varname]
                plt.sca(ax[varname])
                dimensionless = varname in ['d','xeq'] and do_ratio[varname]
                if dimensionless:
                    varplot = var/a
                    varplotmax = varmax/amax
                else:
                    varplot = var/scale[varname]
                    varplotmax = varmax/scale[varname]
                line,=plt.plot(a/ascale,varplot,color=color)
                line.set_label(r'$E_e = '+SP.latex_float(Ee/1e6)+r'$ '+MVm)
                plt.plot(amax/ascale,varplotmax,color=color,marker='o')
                if varname=='Em':
                    # Plot also Ef
                    plt.plot(a/ascale,Ef[ia]/scale[varname],color=color,linestyle='dashed')
                    plt.plot(amax/ascale,resmax['Ef']/scale[varname],color='white',
                             marker='o',markeredgecolor=color)
                plt.grid(True)
                plt.xlabel(r'$a$, '+mm)
                if dimensionless:
                    plt.ylabel(r'$'+symbol[varname]+r'/a$')
                elif varname == 'Em':
                    if LANGUAGE=='eng':
                        plt.ylabel(r'$E_m$ (solid), $E_f$ (dashed), '+unit[varname])
                    elif LANGUAGE=='rus':
                        plt.ylabel(r'$E_m$ (сплошные), $E_f$ (штрих.), '+unit[varname])
                else:
                    plt.ylabel(r'$'+symbol[varname]+r'$, '+unit[varname])
                if do_title:
                    plt.title(r'$L = '+SP.latex_float(L/1e-3)+r'$ '+mm)
    
        for kvar, varname in plot_var:
            plt.sca(ax[varname])
            
            plt.xlim(0,alims[kL]/ascale)
            dimensionless = varname in ['d','xeq'] and do_ratio[varname]
            if dimensionless:
                if varname in dimylim:
                    print('Setting limits for',varname,'to',dimylim[varname])
                    plt.ylim(dimylim[varname])
            elif varname in ylim:
                print('Setting limits for',varname,'to',ylim[varname])
                plt.ylim(*ylim[varname][0],**ylim[varname][1])
    
            if do_plot_legend.get(varname,False):
                plt.legend()
            letter = plot_letter.get(varname,None)
            loc = plot_letter_loc.get(varname,None)
            if letter is not None and loc is not None:
                place_letter(letter, loc=loc)
        plt.pause(.1)
    
    if save_figs:
        for kL,L in enumerate(Lmode):
            for kvar, varname in plot_var:
                fnum1 = 1000*(kL+1) + 11 + kvar
                fig = plt.figure(fnum1)
                fname = prefixa+varname+'_L'+str(SP.neat_float(L/1e-3))+'mm' + save_format
                print('Saving figure',fnum1,'to',fname)
                plt.savefig(fname)
    pass

#%%#################################################################################################
# PART TWO: Plot only maxV results
            
# Load stuff needed for all figures
do_presentation = False

L_in_legend = r'$L=$' if do_presentation else ''
presentation_ext = '_talk' if do_presentation else ''
            
nfigstart = 100
Lplot = np.array([40,120,200])*1e-3
Eelimp = np.array([0,1.4e6])
Eelimn = np.array([0.9e6,2.5e6])
# Now, specialize to the sign
Eelim = Eelimp if sign==1 else Eelimn
kEplot = index_interval(Eelim[0],Eelim[1],resmaxv['Ee'])
Eeplot = resmaxv['Ee'][kEplot]
kLplot = np.searchsorted(resmaxv['L'], Lplot-1e-6)
resplot=dict()
for key in ['V','a','Es','Em','ns','d']:
    resplot[key]=resmaxv[key][kEplot,:][:,kLplot]
kEmode = np.searchsorted(Eeplot, Eemode-1)
kLmode = np.searchsorted(Lplot, Lmode-1e-6)
thelegend = [str(SP.neat_float(L/1e-3))+' '+mm  for kL,L in enumerate(Lplot)]


#styles={1:'solid',2:'dashed',3:'dashdot'}
#Lscale={1:2e3,-1:1e3}


#common_style = {'linestyle':'solid','marker':'o','linewidth':2,'markersize':3}
common_style = {'linestyle':'solid','linewidth':2}

do_add_dots = False
def add_dots(lines,vararr):
    if sign==1 and do_add_dots:
        for kL in kLmode:
            color = lines[kL].get_color()
            plt.plot(Eemode/1e6, vararr[kEmode,kL],'o',markeredgecolor=color,color=color)   

# Clean (no dots), for PRL paper - only V and a for positive streamer
no_dots_ext = '' if (do_add_dots or sign==-1) else '_clean'

#%% Velocity
#thelegend = [str(SP.neat_float(L/1e-3))+' mm'  for kL,L in enumerate(Lfull)]
fig = plt.figure(1+nfigstart)
fig.clear()
ax = fig.add_axes(frame_notitle)
ax.set_xlim(Eelim/1e6)
plotcommand = plt.semilogy if sign==1 else plt.plot
lines = plotcommand(Eeplot/1e6,resplot['V']/1e6,**common_style)
for line,L in zip(lines,Lplot):
    line.set_label(L_in_legend + str(SP.neat_float(L/1e-3))+' '+mm)
add_dots(lines,resplot['V']/1e6)
def AM_Vfit(Ee,u=0,h=0):
    "u is the voltage pulse excess, h is humidity"
    K = 1+(h-11)/100
    Est = (497-16*u)*K*1e3
    vst = (1.25+5*u/100)*1e5
    V = vst*(Ee/Est)**3
    return V
if sign==1:
#    tmp = np.loadtxt('Allen+Mikropoulos1999_Figures/Fig10/curve1.txt')
#    EeAM1 = tmp[:,0]*1e3 + 4000/120e-3
#    vsAM1 = tmp[:,1]*1e5
#    plt.plot(EeAM1/1e6,vsAM1/1e6,'k-.')
#    tmp = np.loadtxt('Allen+Mikropoulos1999_Figures/Fig10/curve2.txt')
#    EeAM1 = tmp[:,0]*1e3 + 2000/120e-3
#    vsAM1 = tmp[:,1]*1e5
#    plt.plot(EeAM1/1e6,vsAM1/1e6,'k:')
#    tmp = np.loadtxt('Allen+Mikropoulos1999_Figures/Fig10/curve3.txt')
#    EeAM1 = tmp[:,0]*1e3
#    vsAM1 = tmp[:,1]*1e5
#    line,=plt.plot(EeAM1/1e6,vsAM1/1e6,'k')
    EeAM = np.linspace(0.4,0.8,101)*1e6
    #vfit = (1.25)
    line,=plt.plot(EeAM/1e6,AM_Vfit(EeAM,0,0)/1e6,'k')
    if do_presentation:
        if LANGUAGE=='eng':
            line.set_label(L_in_legend + '120 mm (exper.)')
        elif LANGUAGE=='rus':
            line.set_label(L_in_legend + '120 мм (эксперим.)')
    else:
        if LANGUAGE=='eng':
            line.set_label('120 mm (experiment)')
        elif LANGUAGE=='rus':
            line.set_label('120 мм (эксперимент)')
plt.grid(True,which='both')
if sign==-1: plt.ylim(0,30)
plt.xlabel(r'$E_e$, '+MVm)
plt.ylabel(r'$V$, '+Mms)
plt.legend()
if not do_presentation:
    place_letter('a',loc='se')
if save_figs:
    figfname = prefix+('compare_' if sign==1 else '') + 'V' + no_dots_ext +\
        presentation_ext + save_format
    print('Saving figure to',figfname)
    plt.savefig(figfname)

#%% Radius
fig = plt.figure(2+nfigstart)
fig.clear()
ax = fig.add_axes(frame_notitle)
ax.set_xlim(Eelim/1e6) #min(Eearr)/1e6,max(Eearr)/1e6)
plotcommand = plt.semilogy if sign==1 else plt.plot
lines = plotcommand(Eeplot/1e6,resplot['a']/1e-3,**common_style)
add_dots(lines,resplot['a']/1e-3)
plt.grid(True,which='both')
plt.xlabel(r'$E_e$, '+MVm)
plt.ylabel(r'$a$, '+mm)
#if sign==1:
#    plt.ylim(0,15)
if sign==-1:
    tmp = plt.ylim()
    plt.ylim(0,tmp[1])
if not do_presentation:
    place_letter('b',loc='se')
if save_figs:
    figfname = prefix+'a' + no_dots_ext + presentation_ext + save_format
    print('Saving figure to',figfname)
    plt.savefig(figfname)

#%% Intrinsic field
if False:
    fig = plt.figure(103+nfigstart)
    fig.clear()
    ax = fig.add_axes(frame_notitle)
    ax.set_xlim(Eelim/1e6) #min(Eearr)/1e6,max(Eearr)/1e6)
    lines = plt.plot(Eeplot/1e6,(Eeplot-resplot['Es'].T).T/1e6,**common_style)
    add_dots(lines,(Eeplot-resplot['Es'].T).T/1e6)
    plt.grid(True)
    plt.xlabel(r'$E_e$, '+MVm)
    plt.ylabel(r'$E_e-E_s$, '+MVm)
    tmp = plt.ylim()
    plt.ylim(0,tmp[1])
    place_letter('c',loc='se')
    #plt.legend(thelegend,loc='lower center')
    if save_figs:
        figfname = prefix+'EeEs' + save_format
        print('Saving figure to', figfname)
        plt.savefig(figfname)


#%% Intrinsic field
fig = plt.figure(3+nfigstart)
fig.clear()
ax = fig.add_axes(frame_notitle)
ax.set_xlim(Eelim/1e6) #min(Eearr)/1e6,max(Eearr)/1e6)
lines = plt.plot(Eeplot/1e6,resplot['Es']/1e6,**common_style)
add_dots(lines,resplot['Es']/1e6)
plt.grid(True)
plt.xlabel(r'$E_e$, '+MVm)
plt.ylabel(r'$E_s$, '+MVm)
if sign==1:
    tmp = plt.ylim()
    plt.ylim(0,tmp[1])
else:
    plt.ylim(0,1.5)
place_letter('c',loc='se')
#plt.legend(thelegend,loc='lower center')
if save_figs:
    figfname = prefix+'Es' + save_format
    print('Saving figure to', figfname)
    plt.savefig(figfname)

#%% Maximum field
fig = plt.figure(4+nfigstart)
fig.clear()
ax = fig.add_axes(frame_notitle)
ax.set_xlim(Eelim/1e6) #min(Eearr)/1e6,max(Eearr)/1e6)
lines = plt.plot(Eeplot/1e6,resplot['Em']/1e6,**common_style)
add_dots(lines,resplot['Em']/1e6)
plt.grid(True)
plt.xlabel(r'$E_e$, '+MVm)
plt.ylabel(r'$E_m$, '+MVm)
tmp = plt.ylim()
if sign==1:
    plt.ylim(0,15)
else:
    plt.ylim(0,15)
#plt.legend(thelegend)
place_letter('d',loc='se')
if save_figs:
    figfname = prefix+'Em' + save_format
    print('Saving figure to',figfname)
    plt.savefig(figfname)

#%% Ionization
fig = plt.figure(5+nfigstart)
fig.clear()
ax = fig.add_axes(frame_notitle)
ax.set_xlim(Eelim/1e6) #min(Eearr)/1e6,max(Eearr)/1e6)
lines = plt.plot(Eeplot/1e6,resplot['ns'],**common_style)
add_dots(lines,resplot['ns'])
plt.grid(True)
plt.xlabel(r'$E_e$, '+MVm)
plt.ylabel(r'$n_s$, '+m3)
tmp = plt.ylim()
if sign==1:
    plt.ylim(0,3e19)
else:
    plt.ylim(0,6e19)
place_letter('e',loc='se') #('ne' if sign==1 else 'se'))
#plt.legend(thelegend,loc='upper center')
if save_figs:
    figfname = prefix+'ns' + save_format
    print('Saving figure to',figfname)
    plt.savefig(figfname)

#%% Front thickness
fig = plt.figure(6+nfigstart)
fig.clear()
ax = fig.add_axes(frame_notitle)
ax.set_xlim(Eelim/1e6) #min(Eearr)/1e6,max(Eearr)/1e6)
da = 100*resplot['d']/resplot['a']
lines = plt.plot(Eeplot/1e6,da,**common_style)
add_dots(lines,da)
plt.grid(True)
plt.xlabel(r'$E_e$, '+MVm)
if LANGUAGE=='eng':
    plt.ylabel(r'$100(d/a)$, dimensionless')
elif LANGUAGE=='rus':
    plt.ylabel(r'$100(d/a)$, безразмерн.')
if sign==1:
    plt.ylim(0,5)
else:
    plt.ylim(0,2)
place_letter('f',loc='se')
#plt.legend(thelegend,loc='lower left')
if save_figs:
    figfname = prefix+'d' + save_format
    print('Saving figure to',figfname)
    plt.savefig(figfname)

#%%#################################################################################################
# PART THREE: Plot the threshlold results
# Must import BOTH signs

resmaxvp = dict(np.load(dirname + 'pos.npz'))
resmaxvn = dict(np.load(dirname + 'neg.npz'))
            
# Must import the correct functions first!
assert SP.nua_fun(2e6)>0
# Number of attenuation lengths
num_attp = -resmaxvp['L']*SP.nut_fun(resmaxvp['Es'])/(resmaxvp['V'] + SP.vd_fun(resmaxvp['Es']))
num_attn = -resmaxvn['L']*SP.nut_fun(resmaxvn['Es'])/(resmaxvn['V'] - SP.vd_fun(resmaxvn['Es']))

#%% The number of attenuation lengths, for either sign
for sign in [1,-1]:
    Eelim = Eelimp if sign==1 else Eelimn
    resmaxv = (resmaxvp if sign==1 else resmaxvn)
    kEplot = index_interval(Eelim[0],Eelim[1],resmaxv['Ee'])
    Eeplot = resmaxv['Ee'][kEplot]
    numattplot = (num_attp if sign==1 else num_attn)[kEplot,:][:,kLplot]
    #num_att[num_att<=0]=np.nan
    fig = plt.figure(11+nfigstart+(0 if sign==1 else 1))
    fig.clear()
    ax = fig.add_axes(frame_title)
    ax.set_xlim(Eelim/1e6) #min(Eearr)/1e6,max(Eearr)/1e6)
    lines = (plt.semilogy if sign==1 else plt.plot)(Eeplot/1e6,numattplot,**common_style)
    if sign==-1: plt.ylim(bottom=0)
    for line,L in zip(lines,Lplot):
        line.set_label(str(SP.neat_float(L/1e-3))+' '+mm)
    plt.grid(True)
    plt.xlabel(r'$E_e$, '+MVm)
    plt.ylabel(r'$L/L_{att}$')
    if sign==1: plt.legend() #loc='upper center')
    place_letter(('a' if sign==1 else 'b'),loc='sw')
    if LANGUAGE=='eng':
        plt.title(('Positive' if sign==1 else 'Negative')+' streamer')
    elif LANGUAGE=='rus':
        plt.title(('Положительный' if sign==1 else 'Отрицательный')+' стример')
    if save_figs:
        figfname = figdirname + ('pos' if sign==1 else 'neg') + '_natt' + save_format
        print('Saving figure to', figfname)
        plt.savefig(figfname)

#%% The thresholds as a function of L
# !!!NEED pip install matplotlib-label-lines
# WARNING: Ethreshn must be calculated separately!
from labellines import labelLine #, labelLines
plot_transp = False # For a presentation, we also plot with transparent negative Eth (True)
plot_Ethreshp = True # default=True
do_LLatt = False # default=False. Choose #attenuation lengths if True, attenuation if False

if plot_Ethreshp:
    if do_LLatt:
        nthreshs = np.array([1,2,5,10,15])
        thresh_styles=['-']*5 #['-','--','-.',':']
        thresh_label_L = [75,100,125,150,175]
    else:
        atten = 1/10**np.arange(3,8,2)
        nthreshs = -np.log(atten)
        thresh_styles=['-.','-',':']
    Larr = resmaxvp['L']
    Ethreshp = np.nan*np.ones((len(Larr),len(nthreshs)))
    for kT, nthresh in enumerate(nthreshs):
        print('Values at L/Latt =',nthresh,':')
        for kL,L in enumerate(Larr):
            Ethreshp[kL,kT] = np.interp(-nthresh,-num_attp[:,kL],resmaxvp['Ee'])
            print('L=',L,': Ethresh =',Ethreshp[kL,kT]/1e6)
    
    # Do not plot for L<20 mm
    if True:
        Ethreshp[Larr<20e-3,:]=np.nan

print('Negative streamer threshold E: calculation is done in sp_Ethreshn.py')
tmp=np.load(dirname + 'Ethresh/Ethreshn.npz')
Ethreshn = tmp['E']

fig = plt.figure(13+nfigstart)
plt.clf()
ax = fig.add_axes(frame_notitle)

if plot_Ethreshp:
    for kT,nthresh in enumerate(nthreshs):
        line, = ax.plot(resmaxv['L']/1e-3, Ethreshp[:,kT]/1e6,
                        color='red',linewidth=2,linestyle = thresh_styles[kT])
        if do_LLatt:
            if kT==0:
                if LANGUAGE=='eng':
                    line.set_label(r'$E_e$ for fixed $L/L_{att}$')
                elif LANGUAGE=='rus':
                    line.set_label(r'$E_e$ для данного $L/L_{att}$')
            #line.set_label(r'$E_{+t}, L/L_{att} = '+str(SP.neat_float(nthreshs[kT]))+r'$')
            labelLine(line,thresh_label_L[kT],
                      #label=r'$L/L_{att} = '+str(SP.neat_float(nthreshs[kT]))+r'$',
                      label=str(SP.neat_float(nthreshs[kT])),
                      ha='left',align=False)
            #           ha='left',va='bottom',align = False)
        else:
            line.set_label(r'$E_{+t}$, $K = '+SP.latex_float(atten[kT]) + r'$')
    
    # The experimental results: two different ways to plot
    if False:
        AMstyles = [dict(color='k',linestyle='--',marker='s',markerfacecolor='w',markeredgecolor='k'),
                    dict(color='k',linestyle='-',marker='s',markerfacecolor='k',markeredgecolor='k'),
                    dict(color='k',linestyle='--',marker='o',markerfacecolor='w',markeredgecolor='k'),
                    dict(color='k',linestyle='-',marker='o',markerfacecolor='k',markeredgecolor='k')]
        for kcurve in range(1,5):
            tmp = np.loadtxt(os.path.join('Allen+Mikropoulos1999_Figures',
                                          'Fig4', 'curve' + str(kcurve) +'.txt'))
            LAM1 = tmp[:,0]*1e-2
            EtAM1 = tmp[:,1]*1e3
            line,=plt.plot(LAM1/1e-3,EtAM1/1e6, **AMstyles[kcurve-1])
            if kcurve==1:
                if LANGUAGE=='eng':
                    line.set_label(r'$E_{+t}$, experim.')
                elif LANGUAGE=='rus':
                    line.set_label(r'$E_{+t}$, эксперим.')
    else:
        # Only "threshold and stability field" for 270 ns (curve 2)
        LtAM = np.array([2,4,6,8,10,11,12])*1e-2
        EetAMs = np.zeros((2,7),dtype=np.double)
        for kcurve in [1,2]:
            tmp = np.loadtxt(os.path.join('Allen+Mikropoulos1999_Figures',
                                          'Fig4', 'curve' + str(kcurve) +'.txt'))
            EetAMs[kcurve-1,:]=tmp[:,1]*1e3
        EetAM = (EetAMs[0]+EetAMs[1])/2
        dEetAM = (EetAMs[1]-EetAMs[0])/2
        line = plt.errorbar(LtAM/1e-3,EetAM/1e6,yerr=dEetAM/1e6,capsize=2,fmt='k.-')
        if LANGUAGE=='eng':
            line.set_label(r'$E_{+t}$, experim.')
        elif LANGUAGE=='rus':
            line.set_label(r'$E_{+t}$, эксперим.')

lines = ax.plot(resmaxvn['L']/1e-3,Ethreshn/1e6, color='blue',
                alpha=(.1 if plot_transp else 1), linewidth=2)
lines[0].set_label(r'$E_{-t}$')

plt.grid(True)
plt.xlabel(r'$L$, '+mm)
if plot_Ethreshp:
    plt.ylabel(r'$E_{\pm t}$, '+MVm)
else:
    plt.ylabel(r'$E_{-t}$, '+MVm)
if plot_Ethreshp:
    plt.ylim(0.1, 1.8)
else:
    plt.ylim(None, None)
plt.xlim(0,200)
if plot_Ethreshp:
    # Need legend only if plotting the positive thresholds, too
    if do_LLatt:
        plt.legend(loc='upper right')
    else:
        plt.legend(loc='center left')

if save_figs:
    if plot_Ethreshp:
        figfname = figdirname + 'thresholds' + \
            ('_LLatt' if do_LLatt else '_atten') + \
            ('_transp' if plot_transp else '') + save_format
    else:
        figfname = 'Ethreshn' + save_format
    print('Saving figure to', figfname)
    plt.savefig(figfname)

#%%#################################################################################################
# PART FOUR: The branches
plot_dot = True
plot_close_gap = True # extrapolate data to close the gap between two negative branches
short_fname = True # True for a simple name to be used in the paper
Ee = 1.5e6
L = 80e-3
Ee_s = str(SP.neat_float(Ee/1e6))
L_s = str(SP.neat_float(L/1e-3))
resn = np.load(dirname + SP.get_fname(-1,Ee,L) + '.npz')
resp = np.load(dirname + SP.get_fname(+1,Ee,L) + '.npz')
resmaxvn1 = SP.apply_maxV(**resn)
resmaxvp1 = SP.apply_maxV(**resp)
fig = plt.figure(666)
fig.clear()
ax = fig.add_axes(frame_title)
lines = plt.plot(resp['a']/1e-3,resp['V'].T/1e6,color='red')
if LANGUAGE=='eng':
    lines[0].set_label('Positive')
elif LANGUAGE=='rus':
    lines[0].set_label('Положительный')
if plot_dot:
    plt.plot(resmaxvp1['a']/1e-3,resmaxvp1['V']/1e6,marker='o',color='red')
lines = plt.plot(resn['a']/1e-3,resn['V'].T/1e6,color='blue')
if LANGUAGE=='eng':
    lines[0].set_label('Negative')
elif LANGUAGE=='rus':
    lines[0].set_label('Отрицательный')
if plot_dot:
    plt.plot(resmaxvn1['a']/1e-3,resmaxvn1['V']/1e6,marker='o',color='blue')
# Close the gap on the left for the negative branches
if plot_close_gap:
    ka = np.where(~np.isnan(resn['V']))[1][0]
    # A tilted parabola. A bit lame algorithm, but it works.
    Va = [resn['V'][0,ka+1],resn['V'][0,ka],resn['V'][1,ka],resn['V'][1,ka+1]]
    a1,a2 = resn['a'][ka],resn['a'][ka+1]
    V2 = (Va[0]+Va[3])/2
    V1 = (Va[1]+Va[2])/2
    dV2 = Va[0]-V2
    dV1 = Va[1]-V1
    C2 = dV2**2
    C1 = dV1**2
    A = np.sqrt((C2-C1)/(a2-a1))
    a0 = (C2*a1-C1*a2)/(C2-C1) # parabola tip
    beta = (V2-V1)/(a2-a1) # parabola tilt
    V0 = (V1*a2-V2*a1+(V2-V1)*a0)/(a2-a1) # parabola tip
    Vx = np.linspace(-dV1,dV1)
    a_new = a0 + (Vx/A)**2
    V_new = Vx + V0 + (a_new-a0)*beta
    plt.plot(a_new/1e-3,V_new/1e6,color='blue')
plt.grid(True)
plt.xlabel(r'$a$, '+mm)
plt.ylabel(r'$V$, '+Mms)
plt.title(r'$E_e='+Ee_s+r'$ '+MVm+', $L='+L_s+r'$ '+mm)
plt.xlim(0,50)
tmp = plt.ylim()
plt.ylim(0,tmp[1])
plt.legend()
if save_figs:
    if short_fname:
        fname =figdirname +'branches'
    else:
        # Give an exhaustive name to the file
        sdot = ('_dot' if plot_dot else '')
        fname = figdirname+'branches' + sdot + '_Ee' + Ee_s + 'MVm_L' + L_s + 'mm'
    fname += ('' if plot_dot else '_nodot') + save_format
    print('Saving figure to',fname)
    plt.savefig(fname)
