# Physics and Mathematics of Electric Streamers

This directory contains scripts necessary to reproduce figures in the paper
* Лехтинен Н.Г. Физика и математика электрических стримеров. _Известия высших учебных заведений. Радиофизика_. (2021). Т. 64. № 1. С. 12-28. [doi:10.52452/00213462_2021_64_01_12](https://dx.doi.org/10.52452/00213462_2021_64_01_12) (in Russian)
* Lehtinen, N.G. Physics and Mathematics of Electric Streamers. _Radiophys Quantum El_ **64**, 11–25 (2021). [doi:10.1007/s11141-021-10108-5](https://dx.doi.org/10.1007/s11141-021-10108-5)

**Python3** is required for these calculations. Other requirements are listed in the [`requirements.txt`](requirements.txt) file

## Do the calculations
If you only need to calculate streamer parameters for a concrete situation, do the following. In this example, we calculate parameters for a positive streamer (`sign=1`), propagating in external field of 1.25 MV/m (`Ee=1.25e6`) and of length 1 cm (`L=10e-3`):
```python
In [1]: import streamer_parameters_v1_2_0 as SPM
*** Streamer Parameter Package, v. 1.2.0 ***
Preferences are set to: EShift=0, ESmooth=False, NEs=200, accurate_ns=True, apha=0.5, mom=True, p_Eout=1
Critical field (minimum of Psi) = 2.8658 MV/m
Max error in ns = 3.0954e+17 m^{-3}

In [2]: param = SPM.calculate_streamer_parameters(sign=1, Ee=1.25e6, L=10e-3)
XXXXXXXXXX

In [3]: param
Out[3]:
{'sign': 1,
 'Ee': 1250000.0,
 'L': 0.01,
 'a': 0.0003652001884547058,
 'V': 238497.11513447587,
 'Es': 716151.4532874551,
 'xim': 3.085238567014207e-05,
 'xeq': 0.0003120145907249604,
 'Exeq': 5207489.445042805,
 'Ef': 14215219.914021358,
 'Em': 10934159.220613,
 'ns': 3.104831843099979e+19,
 'd': 1.1047060567078716e-05}
```
The results are: `a` is the radius, `V` is the velocity, `Es` and `ns` are the field and electron density inside the streamer channel, `Em` is the maximum field at the tip, `d` is the e-folding distance of the electron avalanche at the tip (i.e., the front thickness). The rest of the parameters determine the shape of the electric field at the tip, please read the paper to understand what they mean.

## Reproduce paper results
Run SPM (Streamer Parameter Model) to reproduce the data to be used for plots:
```python
import streamer_parameters_v1_2_0 as SPM
SPM.reproduce_paper()
SPM.calculate_negative_threshold()
```

These functions, however, do not use multiprocessing. To take advantage of multiple processors on your computer, run:
```
python3 calculate_spm.py
```

This will launch several processes. If this calculation is interrupted, before resuming it delete `lock` files:
```
rm -rf spm_work/final_v1_2_0/*/*.lock
```

## Create plots

Plots are produced by the following scripts:

* `python3 plot_nuai.py` - Figure 1
* `python3 plot_sigmoid.py` - Figure 3
* `python3 plot_spm.py` - rest of the Figures

Before running them, edit the information in the beginning of each of these files:
```python
LANGUAGE='eng'        # choose the language of the Figures: 'eng' or 'rus'
save_figs = True      # set to False to skip creating figure files
save_format = '.png'  # figure format
sign = -1             # only in 'plot_spm.py': streamer polarity (1 or -1) to plot
```
