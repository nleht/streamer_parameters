#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 17 10:33:45 2017

Some utilities of questionable usability (some may be reinventions of a wheel)

@author: nle003
"""

import os
import pickle
import numbers
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import io
import scipy.constants as phys
import scipy.special as spec
import scipy.interpolate
import sys
import pkgutil

####################################################################################################
#%% A few general-purpose routines

def make_dir(dirname, verbose=False):
    "Create directory *dirname* with subdirectories, warn if create, do nothing if exists"
    if not os.path.isdir(dirname):
        print(red('Warning:'), 'Creating directory', dirname)
        os.makedirs(dirname)
    elif verbose:
        print(blue('Info:'), 'Using directory', dirname)

def get_size(obj, seen=None):
    """Recursively finds size of objects"""
    size = sys.getsizeof(obj)
    if seen is None:
        seen = set()
    obj_id = id(obj)
    if obj_id in seen:
        return 0
    # Important mark as seen *before* entering recursion to gracefully handle
    # self-referential objects
    seen.add(obj_id)
    if isinstance(obj, dict):
        size += sum([get_size(v, seen) for v in obj.values()])
        size += sum([get_size(k, seen) for k in obj.keys()])
    elif hasattr(obj, '__dict__'):
        size += get_size(obj.__dict__, seen)
    elif isinstance(obj, np.ndarray):
        size += obj.nbytes
    elif hasattr(obj, '__iter__') and not isinstance(obj, (str, bytes, bytearray)):
        size += sum([get_size(i, seen) for i in obj])
    return size

def imports():
    "List all the imported modules"
    import types
    for name, val in globals().items():
        if isinstance(val, types.ModuleType):
            print(val.__name__,'as',name)

def reimport(module):
    """Does not always work. Please try built-in IPython commands, or (still better)
    restart the kernel"""
    import importlib
    for name in dir(module):
        if name[0:2]!='__': delattr(module, name)
    importlib.reload(module)
    
def submodules(package):
    return list(p.name for p in pkgutil.iter_modules(package.__path__))

####################################################################################################
#%% Numerics

def index_interval(xlim1,xlim2,xarr):
    "Index in xarr corresponding to [xlim1,xlim2], but extend by one (for good interpolation)"
    i = (xarr>=xlim1) & (xarr<=xlim2)
    i[1:] |= i[:-1]; i[:-1] |= i[1:]
    return i

def line_interp2d(x,y,xp,yp,zp):
    """Workaround around clumsy 2D interpolation in scipy.
    Stolen from https://stackoverflow.com/a/47233198"""
    shape=x.shape
    assert y.shape==shape
    f = scipy.interpolate.interp2d(xp, yp, zp, bounds_error=True)
    return scipy.interpolate.dfitpack.bispeu(
            f.tck[0], f.tck[1], f.tck[2], f.tck[3], f.tck[4],
            x.flat, y.flat)[0].reshape(shape)

def ones_as(x):
    "Scalar or vector ones"
    return np.ones(np.asarray(x).shape)[()]

def zeros_as(x):
    "Scalar or vector zeros"
    return np.zeros(np.asarray(x).shape)[()]

def anti_diff(arr0,darr):
    "The inverse operation to ``np.diff`` for 1D arrays"
    return np.cumsum(np.hstack((arr0,darr)))

def ave(x):
    """Useful for plotting histograms:
        plt.plot(ave(edges),hist,'.')
    See also: duplicate, meandrize
    """
    return (x[1:]+x[:-1])/2

# NOTE: these two function are not really needed, use drawstyle='steps-post' keyword to plot
def duplicate(x):
    """Useful for plotting histograms:
        plt.plot(meandrize(edges),duplicate(hist))
    An alternative to drawstyle='steps-post' keyword
    """
    return np.tile(np.array([x]).T,2).flatten()

def meandrize(x):
    """Useful for plotting histograms:
        plt.plot(meandrize(edges),duplicate(hist))
    """
    return np.hstack((x[0],duplicate(x[1:-1]),x[-1]))

def make_dispatch(arg,boundaries):
    "Auxiliary function to be used with numpy.choose"
    tmp=np.asarray(arg)
    res=np.zeros(tmp.shape,dtype=np.int)
    for b in boundaries:
        res += (tmp>=b).astype(np.int)
    return res

def dispatch(arg,boundaries,results):
    """Choose results depending on the arg belonging to an interval
    defined by boundaries"""
    return np.choose(make_dispatch(arg,boundaries),results)

def map_to_ndarray(func,ndarr,depth=1,params=(),keywords={}):
    """A more intuitive version of np.vectorize()"""
    if depth==0:
        return func(ndarr,*params,**keywords)
    elif depth>0:
        return np.array([map_to_ndarray(func,subarr,depth=depth-1,
                                        params=params,keywords=keywords)
                        for subarr in ndarr])
    else:
        raise Exception('depth')

#%%#################################################################################################
# Plotting

def fig_save(fig,fname):
    "Save figure `fig` into file `fname`"
    if isinstance(fig, numbers.Integral):
        fig = plt.figure(fig)
    with open(fname,'wb') as f:
        pickle.dump(fig,f)

def fig_open(fname, show=True):
    "Return figure stored in file `fname` in pickle format"
    with open(fname,'rb') as f:
        fig = pickle.load(f)
    if show:
        fig.show()
    return fig

def nice_figures(on=True):
    "Nice-looking figures (sort of)"
    if on:
        #frame_title = [0.13,0.14,0.84,0.77] # left, bottom, width, height
        #frame_notitle = [0.13,0.14,0.84,0.8] # left, bottom, width, height, no space for title
        # Default figure size is 6 x 4 for inline and 8 x 6 for TkAgg
        mpl.rcParams['figure.figsize'] = (6.0, 4.0)
        # Default font size is 10 for inline and 12 for TkAgg
        mpl.rcParams['font.size'] = 12.0 # this line gets ignored
        mpl.rcParams['figure.subplot.bottom'] = 0.14 # default: 0.11,
        #mpl.rcParams['figure.subplot.hspace'] = 0.2,
        mpl.rcParams['figure.subplot.left'] = 0.13 # default: 0.125,
        mpl.rcParams['figure.subplot.right'] = 0.97 # default: 0.9,
        mpl.rcParams['figure.subplot.top'] = 0.94 # default: 0.88
        #mpl.rcParams['figure.subplot.wspace'] = 0.2,
        mpl.rcParams['axes.grid'] = True # default: False,
        mpl.rcParams['axes.grid.axis'] = 'both' # default: 'both',
        mpl.rcParams['axes.grid.which'] = 'both' # default: 'major',
    else:
        # Restore defaults
        for k,v in mpl.rcParams.items():
            vd = mpl.rcParamsDefault[k]
            if v!=vd and k!='interactive':
                #print(k,v,vd)
                mpl.rcParams[k] = (vd.copy() if isinstance(vd,list) else vd)

def place_text(x,y,text,size=30,ax=None):
    "Determine the limits from x, y in [0,1] and scale and place at the correct coordinates"
    if ax is None:
        ax = plt.gca()
    def convert(z01,sz,z):
        z0,z1 = z01
        if sz=='linear':
            zs = z0 + z*(z1-z0)
        elif sz=='log':
            zs = np.exp(np.log(z0)+z*np.log(z1/z0))
        else:
            raise Exception('scale')
        return zs
    plt.text(convert(ax.get_xlim(),ax.get_xscale(),x),
             convert(ax.get_ylim(),ax.get_yscale(),y),text,fontsize=size)

def place_letter(t,loc='se'):
    if loc[0]=='s':
        ytext = 0.03
    elif loc[0]=='n':
        ytext = 0.85
    else:
        raise Exception('uknown south or north')
    if loc[1]=='e':
        xtext = 0.88
    elif loc[1]=='w':
        xtext = 0.02
    else:
        raise Exception('uknown east or west')
    place_text(xtext, ytext, '('+t+')')

def plt_no_offsets():
    plt.gca().get_xaxis().get_major_formatter().set_useOffset(False)
    plt.gca().get_yaxis().get_major_formatter().set_useOffset(False)
    plt.draw()

def reset_colors():
    plt.gca().set_prop_cycle(None)

####################################################################################################
#%% Terminal colors and pretty printing
# See https://www.digitalocean.com/community/tutorials/how-to-customize-your-bash-prompt-on-a-linux-vps
# \e -- same as \033 is escape char
# \e[ -- start of color info
# m\] -- end of color info
# Color info: 0,1,4 - normal,bold or underlined text; 3x -- foreground color; 4x -- background color, where
# x = 0-7 is black, red, green, yellow, blue, purple, cyan, white
# \[\e]0; -- start of the terminal title (not displayed in the normal prompt)
# \a\] -- end of the terminal title

__colors = {} # will not get visible imported by from utils import *
def colorful_print(use_colors=True):
    "Make a colorful terminal output. Use set_colors(False) to turn it off."
    global __colors
    if use_colors:
        __colors = {'black': '\x1b[30m',
                    'red': '\x1b[31m',
                    'green': '\x1b[32m',
                    'yellow': '\x1b[33m',
                    'blue': '\x1b[34m',
                    'magenta': '\x1b[35m',
                    'cyan': '\x1b[36m',
                    'white': '\x1b[37m',
                    'end': '\x1b[0m',
                    'bold': '\x1b[1m',
                    'underline': '\x1b[4m'}
    else:
        __colors = {'black':'', 'red':'', 'green':'', 'yellow':'',
                  'blue':'', 'magenta':'', 'cyan':'', 'white':'',
                  'end':'', 'bold':'', 'underline':''}

colorful_print()

def get_color(arg):
    "May be unnecessary"
    return __colors[arg]

def colorize(s,color=None,bold=False,underline=False):
    if color is not None:
        s = s + get_color('end')
        if bold:
            s = get_color('bold') + s
        if underline:
            s = get_color('underline') + s
        s = get_color(color) + s
    return s

def red(s):
    return colorize(s,color='red',bold=True)

def blue(s):
    return colorize(s,color='blue',bold=True)

def string(*a,**kw):
    "Python3-style print into a string"
    s=io.StringIO()
    kw.setdefault('file',s)
    kw.setdefault('end','')
    print(*a,**kw)
    tmp=s.getvalue()
    s.close()
    return tmp

class neat_float(float):
    "Short-print of floats"
    def __str__(self):
        return "%0.5g" % self.real
    def __repr__(self):
        return self.__str__()

def latex_float(f,digits=5):
    s = '-' if (f<0) else ''
    float_str = ('{0:.'+str(int(abs(digits)))+'g}').format(abs(f))
    if "e" in float_str:
        base, exponent = float_str.split("e")
        basef = '' if base=='1' else r'{:} \times'.format(base)
        return s + basef + r'10^{{{:}}}'.format(int(exponent))
    else:
        return s + float_str

def string_header(*a,color=None,bold=False,linewidth=None,fill='*',**print_kw):
    s = string(*a,**print_kw)
    if linewidth is None:
        linewidth=np.get_printoptions()['linewidth']
    if len(s)==0:
        s = fill*linewidth
    else:
        if len(s)>linewidth-2:
            return s
        n1 = (linewidth-2-len(s))//2
        n2 = linewidth-2-len(s)-n1
        s = fill*n1 + ' ' + s + ' ' + fill*n2
    return colorize(s,color=color,bold=bold)

def print_header(*a,**b):
    print(string_header(*a,**b))

def print_big_header(*a,color=None,bold=False,linewidth=None,fill='*',**print_kw):
    if linewidth is None:
        linewidth=np.get_printoptions()['linewidth']    
    print(get_color(color) + (get_color('bold') if bold else '') + fill*linewidth)
    print(string_header(*a,linewidth=linewidth,fill=fill,**print_kw))
    print(fill*linewidth + get_color('end'))
    
def nice_print(on=True):
    "NumPy output suited for the wide screen in Spyder"
    if on:
        np.set_printoptions(edgeitems=100, linewidth=180, threshold=10000)
    else:
        np.set_printoptions(edgeitems=3, linewidth=75, threshold=1000)
    
####################################################################################################
#%% A base class for cached calculations
# Things to do: remote control

class cached_calculator:
    """Persistent object for caching calculations.
    We want it to be flexible so that it can load/save files when needed.
    
    Example of usage:
    -----------------
    >>> import time
    ... class dumb_calculator(cached_calculator):
    ...     "These two functions must have the same signatures!"
    ...     def file_name(self,x,progress=False):
    ...         return 'dumb'+str(x)
    ...     def calculate(self,x,progress=False):
    ...         if progress:
    ...             for k in range(10):
    ...                 print('X',end='',flush=True)
    ...                 time.sleep(1.0)
    ...             print(' ... finished!',end='',flush=True)
    ...         return {'y':x**2,'z':'az'}
    ... f = dumb_calculator(one_liner=False) # for very long calculations, displaying lot out output
        
    It can then be used in the following manner:
    
    >>> x = f.verbose.nocache(5,progress=True) # acts as a regular function, do not read file
    >>> y = f.verbose.noload(5) # creates a file 'dumb5.npz' but returns None if it already exists
    >>> z = f.verbose(5) # regular cached calculation
    >>> t = f.verbose.erase(5) # recalculate
    >>> u = f.verbose.nocalc(5) # if the file does not exist, return None, otherwise read from file
        
    It is possible to do without subclassing:
        
    >>> f = cached_calculator() 
    >>> f.file_name = lambda x: 'dumb'+str(x)
    >>> f.calculate = lambda x: {'y':x**2,'z':'az'}
    
    and use it in exactly the same manner as above.
    """
    def __init__(self,one_liner=True):
        self.one_liner = one_liner
        self.__restore()
    "The following two functions are vitual and must be overloaded and must have same signatures."
    def file_name(self,*args,**kws):
        raise NotImplementedError()
    def calculate(self,*args,**kws):
        "Implementation is responsible for displaying progress if self.progress==True"
        raise NotImplementedError()
    "Several functions that change the defaults"
    @property
    def erase(self):
        "Erase the existing cache. Should be used only if calculation algorithm changed."
        self.__erase = True # erase existing cache (force recalculation)
        return self
    @property
    def noload(self):
        "Skip loading the file if it exists, but calculate and create the cache otherwise"
        self.__noload = True # if file exists, just skip it (do not return anything)
        return self
    @property
    def nocache(self):
        "Make it behave just like a regular function"
        self.__nocache = True
        return self
    @property
    def verbose(self):
        self.__verbose = True
        return self
    def lock(self,s):
        self.__lock = s
        return self
    @property
    def nocalc(self):
        self.__nocalc = True
        return self
    def __restore(self):
        "Defaults"
        self.__erase = False # do not erase existing cache
        self.__noload = False # do not skip loading data from file
        self.__nocache = False
        self.__verbose = False
        self.__lock = 'lock'
        self.__nocalc = False
    def __return(self,result=None):
        self.__restore()
        return result
    def __call__(self,*args,**kws):
        """This class is written for the sake of this wrapper."""
        prefix = self.file_name(*args,**kws)
        if not self.__nocache:
            fname = prefix + '.npz'
            flock = prefix + '.' + self.__lock
            if os.path.isfile(flock):
                if self.__verbose:
                    print('Skipping locked',fname)
                return self.__return()
            if not self.__erase and os.path.isfile(fname):
                if self.__verbose:
                    print('Not calculating',fname,end=' ')
                if self.__noload:
                    # skip loading the file
                    if self.__verbose: print('(not loading)')
                    result = None
                else:
                    if self.__verbose: print('(loading)')
                    result = dict(np.load(fname,allow_pickle=True))
                return self.__return(result)
            if self.__nocalc:
                if self.__erase:
                    print(red('Warning:'),'incompatible options erase and nocalc, ignoring erase')
                # File not found
                if self.__verbose:
                    print('File not found:',fname)
                return self.__return()
        # Now, we have to do calculations
        if self.__nocache:
            if self.__noload and self.__verbose:
                print(red('Warning:'),'ignoring noload option')
            message=red('(not saving!)')
        else:
            with open(flock, 'w') as f:
                f.write('')
            message=''
        print('Calculating',prefix,message,'...',end=(' ' if self.one_liner else '\n'))
        result = self.calculate(*args,**kws) # all calculation is done here
        for k,v in result.items():
            result[k] = np.asarray(v)
        if not self.__nocache:
            np.savez(fname,**result)
            os.remove(flock)
        if self.one_liner:
            print('done!')
        else:
            print('')
        return self.__return(result)
    pass


