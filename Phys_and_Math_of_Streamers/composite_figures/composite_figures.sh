#!/bin/sh
latex pos_results.tex
dvips pos_results.dvi -E -o pos_results.eps
# If want to compress
# epstool --copy --bbox pos_results.eps pos_results_new.eps
latex neg_results.tex
dvips neg_results.dvi -E -o neg_results.eps

