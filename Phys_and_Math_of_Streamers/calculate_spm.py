#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 21 15:06:14 2021

Do the long calculations for SPM (Streamer Parameter Model).

This may be launched simultaneously many times from the same directory.

@author: Nikolai G. Lehtinen
"""

import os
import time
#import subprocess as sp
import multiprocessing as mp
from contextlib import redirect_stdout

import streamer_parameters_v1_2_0 as SP

def calculation_1():
    with open(os.devnull, "w") as null_stream:
        with redirect_stdout(null_stream):
            SP.mkdir('spm_work')
            os.chdir('spm_work')
            SP.reproduce_paper()

def calculation_2():
    with open(os.devnull, "w") as null_stream:
        with redirect_stdout(null_stream):
            SP.mkdir('spm_work')
            os.chdir('spm_work')
            SP.calculate_negative_threshold()


Nproc = mp.cpu_count()

print('Num processors =', Nproc)

procs = []
for i in range(Nproc):
    #proc = sp.Popen(['python3', 'calculate_spm_1.py',], stdout=sp.DEVNULL, stderr=sp.DEVNULL)
    # stdout=sp.PIPE, stderr=sp.PIPE -- does not work
    proc = mp.Process(target=calculation_1)
    time.sleep(0.1)
    proc.start()
    procs.append(proc)

for proc in procs:
    #proc.wait()
    proc.join()

print('Done with modes, now the negative thresholds')

procs = []
for i in range(Nproc):
    #proc = sp.Popen(['python3', 'calculate_spm_2.py',], stdout=sp.DEVNULL, stderr=sp.DEVNULL)
    proc = mp.Process(target=calculation_2)
    time.sleep(0.1)
    proc.start()
    procs.append(proc)

for proc in procs:
    #proc.wait()
    proc.join()
