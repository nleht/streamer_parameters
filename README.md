# Streamer Parameter Model

Python3 code which implements the novel computationally-effective method to calculate electric streamer parameters, such as its radius and speed, given the length and external field configuration.

The Streamer Parameter Model (SPM) is described in the following papers (which **must** be cited if you would like to run it for your publication or presentation):

1. Лехтинен Н.Г. Физика и математика электрических стримеров. _Известия высших учебных заведений. Радиофизика_. (2021). Т. 64. № 1. С. 12-28. [doi:10.52452/00213462_2021_64_01_12](https://dx.doi.org/10.52452/00213462_2021_64_01_12) (in Russian)
2. Lehtinen, N.G. Physics and Mathematics of Electric Streamers. _Radiophys Quantum El_ **64**, 11–25 (2021). [doi:10.1007/s11141-021-10108-5](https://dx.doi.org/10.1007/s11141-021-10108-5)

Materials for these papers are in [this subdirectory](Phys_and_Math_of_Streamers). The [README](Phys_and_Math_of_Streamers/README.md) file contains instructions on how to calculate the parameters for a streamer of given sign and length in given external field.

More details can be found in manuscript [*Electric streamers as a nonlinear instability: the model details*](https://arxiv.org/abs/2003.09450). However, if you would like to use any material from it, papers **1** or **2** must be cited.

More papers on this topic:

3. Lehtinen, N.G.; Marskar, R. What Determines the Parameters of a Propagating Streamer: A Comparison of Outputs of the Streamer Parameter Model and of Hydrodynamic Simulations. _Atmosphere_ **12**, 1664 (2021). [doi:10.3390/atmos12121664](https://doi.org/10.3390/atmos12121664)

   Materials for this paper are in [this subdirectory](Comparison_to_Bagheri_et_al_2018).
