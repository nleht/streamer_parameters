Simulation results extracted from the Figures in Bagheri et al (2018) using software WebPlotDigitizer, v.4.5, see https://automeris.io/WebPlotDigitizer/

Except radius data for Case 1, which is not plotted explicitly in Bagheri et al (2018). We extracted the approximate values of the radius from Figure 3 (only CWI group).

