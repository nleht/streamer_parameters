# What Determines the Parameters of a Propagating Streamer: A Comparison of Outputs of the Streamer Parameter Model and of Hydrodynamic Simulations

This directory contains scripts necessary to reproduce figures in the paper
* Lehtinen, N.G.; Marskar, R. [What Determines the Parameters of a Propagating Streamer: A Comparison of Outputs of the Streamer Parameter Model and of Hydrodynamic Simulations](https://www.mdpi.com/2073-4433/12/12/1664). _Atmosphere_ **12**, 1664 (2021). [doi:10.3390/atmos12121664](https://doi.org/10.3390/atmos12121664)

**Python3** is required for these calculations. Other requirements are listed in the [`requirements.txt`](requirements.txt) file. To add the panel letter (for multi-panel figures) to plots, you will need [`utils.py`](https://gitlab.com/nleht/small-projects/-/blob/28259be084917c009aa48c0e1b55756db3710f55/Utilities/utils.py) from my other [project](https://gitlab.com/nleht/small-projects).


In order to reproduce the figures: run `python3 Bagheri_et_al2018_comparison.py`. You may change some options in the beginning of the file:
```python
save_fig = True           # save figures?
save_format = 'pdf'       # figure format
do_calculations = True    # Make sure to set this to True to perform the necessary calculations
add_panel_letter = True   # add the panel letter, disable if you do not have "utils.py"
```

To produce multi-panel figures, use files in folder [`for_figures`](for_figures).

