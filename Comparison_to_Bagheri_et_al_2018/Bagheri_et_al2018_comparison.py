#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar  1 21:42:40 2020

Check Bagheri result
# Comparison to "six-model" paper, case 3

@author: nleht
"""

#%% Import necessary packages
save_fig = True
save_format = 'pdf'
do_calculations = False
add_panel_letter = True # set to False if package "utils" is not available

import os
#from subprocess import Popen, DEVNULL
import multiprocessing as mp
from contextlib import redirect_stdout
import time
import numpy as np
import scipy.constants as phys
import matplotlib.pyplot as plt
import matplotlib as mpl
from parse import parse
import streamer_parameters_v1_3_0Bagheri_et_al2018 as SP
if add_panel_letter:
    from utils import place_letter

def read_bagheri_data(casenum, varname):
    "Read the data which were created by WebPlotDigitizer"
    if casenum==1:
        if varname=='a':
            teams = ['CWI']
        else:
            teams = ['CWI', 'ES', 'FR', 'TUE', 'CN', 'DE']
    else:
        teams = ['CWI', 'ES', 'FR', 'CN', 'DE']
    fname = os.path.join('bagheri_data','bagheri_case'+str(casenum)+'_'+varname+'.txt')
    print('Reading file', fname)
    data = {team:[] for team in teams}
    with open(fname,'r') as f:
        txt = f.read()
    lines = txt.split('\n')
    teamit = iter(teams)
    team = next(teamit)
    finished = False
    for line in lines:
        tmp = parse('{x:g}, {y:g}', line)
        #print(tmp)
        if tmp is None:
            try:
                team = next(teamit)
                #print(team)
            except StopIteration:
                finished = True
        else:
            if finished:
                raise ValueError('File is too long')
            data[team].append((tmp['x'], tmp['y']))
    for team in teams:
        data[team] = np.array(data[team])
    return data

# Inputs for the Streamer Parameter Model
case_list = ['default', 'high_n_bg', 'low_n_bg', 'Luque', 'Bourdon2', 'Bourdon3']
dirnamebase = 'Bagheri_et_al_2018'
Eearr = np.array([1.5e6]) # fixed
Larr = np.arange(1,12.5, .5)*1e-3
sign = 1 # positive only
dirname_ = os.path.join(dirnamebase,'results_v' + SP.VMarker + '_')

def calculate_res(thecase, diffusion=False):
    "Calculations with Streamer Parameter Model"
    SP.Prefs.reset() # just in case
    SP.Prefs.diffusion = diffusion
    if thecase=='default':
        SP.Photo = SP.PhotoionizationClass('Zheleznyak', 60/(60+760))
        SP.VFinder = SP.PancheshnyiSolverClass(0)
    elif thecase=='high_n_bg':
        SP.Photo = SP.PhotoionizationClass('None', 0)
        SP.VFinder = SP.PancheshnyiSolverClass(1e13)
    elif thecase=='low_n_bg':
        SP.Photo = SP.PhotoionizationClass('None', 0)
        SP.VFinder = SP.PancheshnyiSolverClass(1e9)
    elif thecase=='Luque':
        SP.Photo = SP.PhotoionizationClass('Luque', 40/(40+phys.bar/phys.torr))
        SP.VFinder = SP.PancheshnyiSolverClass(1e9)
    elif thecase=='Bourdon2':
        SP.Photo = SP.PhotoionizationClass('Bourdon2', 40/(40+phys.bar/phys.torr))
        SP.VFinder = SP.PancheshnyiSolverClass(1e9)
    elif thecase=='Bourdon3':
        SP.Photo = SP.PhotoionizationClass('Bourdon3', 40/(40+phys.bar/phys.torr))
        SP.VFinder = SP.PancheshnyiSolverClass(1e9)
    else:
        raise ValueError('Unknown case: '+thecase)
    dirname = dirname_ + thecase + ('_D' if diffusion else '')
    SP.mkdir(os.path.join(dirname, 'modes'))
    # The long calculations
    SP.batch_streamer_modes(sign,Eearr, Larr, dirname)
    # Make summary (ideally, must first check for locks)
    spm_result = SP.batch_apply_maxV(sign, Eearr, Larr, dirname)
    fname_resmaxv = os.path.join(dirname, SP.sign_str(sign) + '.npz')
    np.savez(fname_resmaxv, **spm_result)

def load_res(thecase, diffusion=False):
    dirname = dirname_ + thecase + ('_D' if diffusion else '')
    fname = os.path.join(dirname, SP.sign_str(sign) + '.npz')
    return dict(np.load(fname))

def calculate_everything():
    null_stream = open(os.devnull, "w")
    with redirect_stdout(null_stream):
        for thecase in case_list:
            calculate_res(thecase, diffusion=False)
            calculate_res(thecase, diffusion=True)


#%% The main process
if __name__=='__main__':
    
    #%% The long calculation is here
    if do_calculations:
        # Launch many processes
        Nproc = mp.cpu_count()
        print('Launching', Nproc,'processes')
        procs = []
        for k in range(Nproc):
            #proc = Popen(['python3', 'the_process.py'], stdout=DEVNULL, stderr=DEVNULL)
            proc = mp.Process(target=calculate_everything)
            time.sleep(0.1)
            proc.start()
            procs.append(proc)
        for proc in procs:
            #proc.wait()
            proc.join()
        print('Finished!')
        # What we had:
        # for thecase in case_list:
        #     calculate_res(thecase, diffusion=False)
        #     calculate_res(thecase, diffusion=True)
        
    #%% Set up plotting
    # Default figure size is 6 x 4 for inline and 8 x 6 for TkAgg
    mpl.rcParams['figure.figsize'] = (6.0, 4.0)
    # Default font size is 10 for inline and 12 for TkAgg
    mpl.rcParams['font.size'] = 12.0 # this line gets ignored
    mpl.rcParams['figure.subplot.bottom'] = 0.14 # default: 0.11,
    #mpl.rcParams['figure.subplot.hspace'] = 0.2,
    mpl.rcParams['figure.subplot.left'] = 0.13 # default: 0.125,
    mpl.rcParams['figure.subplot.right'] = 0.97 # default: 0.9,
    mpl.rcParams['figure.subplot.top'] = 0.92 # default: 0.88
    #mpl.rcParams['figure.subplot.wspace'] = 0.2,
    mpl.rcParams['axes.grid'] = True # default: False,
    mpl.rcParams['axes.grid.axis'] = 'both' # default: 'both',
    mpl.rcParams['axes.grid.which'] = 'both' # default: 'major',
    frame_title = [0.14,0.14,0.83,0.77] # left, bottom, width, height
    frame_notitle = [0.14,0.14,0.83,0.8] # left, bottom, width, height, no space for title

    #%% Load stuff
    spms = {thecase:load_res(thecase) for thecase in case_list}
    spms_D = {thecase:load_res(thecase, True) for thecase in case_list}
    
    # Load the results if we have not calculated them above
    for spm in list(spms.values()) + list(spms_D.values()):
        # Calculate Ne
        ns_A = spm['ns']*np.pi*spm['a']**2/2
        spm['Ne'] = ns_A*Larr # Total number of electrons
        spm['I'] = phys.e*ns_A*SP.vd_fun(spm['Es']) # Current
        dL = np.diff(spm['L'])
        dt = dL/SP.ave(spm['V'][0])
        spm['dt'] = dt
        Q = -np.flip(SP.anti_diff(0, np.flip(SP.ave(spm['I'][0])*dt)))
        spm['Q'] = SP.full_as(spm['V'], np.nan)
        spm['Q'][0] = Q - np.nanmin(Q)
    
    # Figure 14a or 16, kV/cm
    dirnamefig = os.path.join(dirnamebase,'figures')
    SP.mkdir(dirnamefig)
    #descriptions = [('a',1e-2,'a','cm',1), ('Em',1e5,'E_m','kV/cm',2),
    #                ('Ne',1e12,'N_e/10^{12}','dimensionless',3),('V',1e6,'V','mm/ns',4)]
    
    descriptions = {'a':(1e-3,'a','mm',1e-2), 'Em':(1e6,'E_m','MV/m',1e5),
                    'Ne':(1e12,'N/10^{12}','dimensionless',1e12),
                    'Lvt':(1e-2,'L-vt','cm',1e-2), 'Es':(1e6,'E_m','MV/m',np.nan),
                    'Q':(1e-9,'Q','nC',1e-9)}

    #%% Plotting functions
    
    def get_tL(spm):
        L = spm['L'].copy()
        V = spm['V'][0] # the first branch
        dL = np.diff(L)
        dt = dL/SP.ave(V)
        #dt[np.isnan(dt)]=0
        t = -np.flip(SP.anti_diff(0,np.flip(dt)))
        t -= np.interp(0.0029, L, t) # moment when L=0.29 cm
        return t, L
    
    def plot_parameter(casenum, varname, fignum=None, legend=False, label_diff=True,
                       letter=None, loc=None, title=None):
        fig = plt.figure(fignum)
        fig.clear()
        if title is None:
            ax = fig.add_axes(frame_notitle)
        else:
            ax = fig.add_axes(frame_title)
        v0=[0.5e6, 0.3e6, 0.6e6]
        if casenum==1:
            casenames = ['high_n_bg']
        elif casenum==2:
            casenames = ['low_n_bg']
        elif casenum==3:
            casenames = ['Luque', 'Bourdon2', 'Bourdon3']
        else:
            raise ValueError('Unknown case number: '+str(casenum))
        scale, vname, units, scale_hds = descriptions[varname]
        if varname in ['a', 'Em', 'Ne', 'Lvt']: # and not (varname=='a' and casenum==1):
            data = read_bagheri_data(casenum, varname)
            for team in data.keys():
                x, y = data[team].T
                line, = ax.plot(x, y*scale_hds/scale, 'kx-', alpha=0.5)
                if team=='CWI':
                    line.set_label('HDS')
        else:
            data = None
        for diff in [False, True]:
            ax.set_prop_cycle(None)
            style = ('--' if diff else '-')
            for casename in casenames:
                spm = (spms_D[casename] if diff else spms[casename])
                if varname=='Lvt':
                    t, L = get_tL(spm)
                    L[t<=0] = np.nan
                    v = v0[casenum-1]
                    line, = ax.plot(t/1e-9,(L-v0[casenum-1]*t)/1e-2, style, linewidth=2) # Figure 13
                else:
                    ii = Larr>=0.003
                    line, = ax.plot(Larr[ii]/1e-2, spm[varname][0][ii]/scale, style, linewidth=2)
                if not diff or label_diff:
                    line.set_label('SPM'+(' '+casename if len(casenames)>1 else '') +
                           (' with diffusion' if diff else ''))
        if varname=='Lvt':
            ax.set_xlabel('$t$, ns')
            ax.set_ylabel(r'$L-vt$, cm, $v='+SP.latex_float(v/1e6)+'$ mm/ns')
            ax.set_xlim(0,None)
        else:
            ax.set_xlabel('$L$, cm')
            ax.set_ylabel(r'$' + vname + r'$, ' + units)
        if legend:
            ax.legend()
        if letter is not None:
            if add_panel_letter:
                place_letter(letter, loc=loc)
            else:
                print('[not adding letter (', letter, ') in location=', loc, sep='')
        if title is not None:
            ax.set_title(title)
        if save_fig:
            fname = 'case' + str(casenum) + '_' + varname
            print('Saving figure', fname)
            fig.savefig(os.path.join(dirnamefig, fname + '.' + save_format))
    
    #%%#############################################################################################
    # Plot everything
    plt.close('all')
    plot_parameter(1, 'Lvt', legend=True, letter='a')  # Fig 5b
    plot_parameter(1, 'Em', letter='b')                # Fig 6a
    plot_parameter(1, 'Ne', letter='c')                # Fig 6b
    plot_parameter(1, 'a', letter='d')
    
    plot_parameter(2, 'Lvt', legend=True, letter='a', loc='sw') # Fig 8
    plot_parameter(2, 'Em', letter='b')                         # Fig 9a
    plot_parameter(2, 'Ne', letter='c')                         # Fig 9b
    plot_parameter(2, 'a', letter='d')                          # Fig 10
    
    plot_parameter(3, 'Lvt', legend=True, label_diff=False, letter='a')  # Fig 13
    plot_parameter(3, 'Em', label_diff=False, letter='b')                # Fig 14a
    plot_parameter(3, 'Ne', label_diff=False, letter='c')                # Fig 14b
    plot_parameter(3, 'a', label_diff=False, letter='d')                 # Fig 15
    
    #%% Extra figures
    if False:
        # Plot the field in the channel
        plot_parameter(1, 'Es', legend=True, letter='a', title='Case 1')
        plot_parameter(2, 'Es', legend=True, letter='b', title='Case 2')
        plot_parameter(3, 'Es', legend=True, label_diff=False,
                       letter='c', loc='nw', title='Case 3')

        plot_parameter(1, 'Q', legend=True, letter='a', title='Case 1')
        plot_parameter(2, 'Q', legend=True, letter='b', title='Case 2')
        plot_parameter(3, 'Q', legend=True, label_diff=False,
                       letter='c', title='Case 3')

    #%% Analysis of the spatial scales in Case 2: low background electron density
    def get_Ng(spm, L):
        Em = np.interp(L, spm['L'], spm['Em'][0])
        Ef = np.interp(L, spm['L'], spm['Ef'][0])
        V = np.interp(L, spm['L'], spm['V'][0])
        vdm = SP.vd_fun(Em)
        d = np.interp(L, spm['L'], spm['d'][0])
        dalt = (V+vdm)/SP.nut_fun(Em)
        d0 = vdm/SP.nut_fun(Em)
        print('At L =', L/1e-2, 'cm : d0 =', d0/1e-6, 'um; d is in [',
              d/1e-6, ',', dalt/1e-6, '] um for Em =',
              Em/1e6, 'MV/m')
        Ng = V/vdm+1
        print('V =', V/1e6, 'mm/ns, vdm =', vdm/1e6, 'mm/ns, Ng =', Ng)
        return Ng
    
    spm1 = load_res('high_n_bg', diffusion=False)
    spm2 = load_res('low_n_bg', diffusion=False)
    spm3 = load_res('Bourdon3', diffusion=False)
    L1 = 0.0035
    L2 = 0.01
    
    for casenum, spm in enumerate([spm1, spm2, spm3]):
        print('Case', casenum+1)
        get_Ng(spm, L1)
        get_Ng(spm, L2)
        